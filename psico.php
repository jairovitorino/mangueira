<?php
session_start();

 include "cabecalho/cabecalho.php";
 $msg_sucesso = @$_SESSION['msg_sucesso'];
 $msg_excessao = @$_SESSION['msg_excessao'];

	$id_psico = @$_SESSION['id_psico'];
	$id_associado = @$_SESSION['id_associado'];
	$nm_associado = @$_SESSION['nm_associado'];
	$te_imagem = @$_SESSION['te_imagem'];
	$te_imagem = substr($te_imagem,13);
	$id_categoria = @$_SESSION['id_categoria'];
	$nm_categoria = @$_SESSION['nm_categoria'];
	$id_psicanalista = @$_SESSION['id_psicanalista'];
	$nm_psicanalista = @$_SESSION['nm_psicanalista'];
	$id_sexo = @$_SESSION['id_sexo'];
	$id_sexo == 1 ? $nm_sexo = "M" : $nm_sexo = "F";
	$nu_rg = @$_SESSION['nu_rg'];
	$dt_inicio = @$_SESSION['dt_inicio'];
	$dt_fim = @$_SESSION['dt_fim'];
	$hr_psico = @$_SESSION['hr_psico'];
	$nm_psico = @$_SESSION['nm_psico'];
	$te_obs = @$_SESSION['te_obs'];
	
	if ( $te_imagem != ""){
	$te_imagem = $te_imagem;
	} else if ( $te_imagem == "" && $id_sexo == 1){
	$te_imagem = "img_masc.jpg";
	} else {
	$te_imagem = "img_fem.jpg";
	}	
   
 $login_mangueira = @$_SESSION['login_mangueira'];
   
 if ($login_mangueira){
?>
<html>
<head>
<title>Contr&ocirc;le acad&ecirc;mico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">

function caixas(){
	//document.getElementById('dt_ini').focus();
	document.formulario.dt_inicio.focus();
}
function executar(delUrl) { 
    document.location = delUrl; 
}

/*function alteraMaiusculo(){
	var valor = document.getElementById("campo").nm_macom;
	var novoTexto = valor.value.toUpperCase();
	valor.value = novoTexto;
	}*/
function del(delUrl) {
  if (confirm("Deseja excluir?")) {
    document.location = delUrl;
  }
}

function foto(){
var saida = document.getElementById('te_imagem');
saida.value =  document.getElementById('upload').value;
}
function barra(objeto){
if (objeto.value.length == 2 || objeto.value.length == 5 ){
objeto.value = objeto.value+"/";
}
}
function barra_hora(objeto){
if (objeto.value.length == 2 || objeto.value.length == 2 ){
objeto.value = objeto.value+":";
}
}
function combo_psico(){
var i = document.getElementById("id_psicanalista").options[document.getElementById("id_psicanalista").selectedIndex].text;
   document.getElementById("nm_psicanalista").value = i;
}
function combo_categoria(){
var i = document.getElementById("id_categoria").options[document.getElementById("id_categoria").selectedIndex].text;
   document.getElementById("nm_categoria").value = i;
}
</script>
</head>
<link rel="stylesheet" href="css/filadelfia.css" type="text/css">
<link rel="SHORTCUT ICON" href="<?php echo $icon;?>"/>
<body bgcolor="#F5F5F5" onLoad="javascript:caixas()">
<?php if ( empty($id_psico) ){?>
<form name="formulario" id="formulario" method="post" action="controller/psico_controller.php" enctype="multipart/form-data" >
  
  <table width="53%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td colspan="2" class="labelCentro">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="labelCentro">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="labelCentro">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="labelCentro">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="labelEsquerda">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="labelEsquerda"> 
        <?php
  if ($msg_sucesso){
   echo "<div align='left'>
   <img src='img/msg_azul.png' width='20' height='20'><font color='#0099CC' size='2' face='Arial, Helvetica, sans-serif'> $msg_sucesso </font></div>";
     } else if ($msg_excessao){
	   echo "<div align='left'>
   <img src='img/msg_vermelha.gif' width='20' height='20'><font color='#FF0000' size='2' face='Arial, Helvetica, sans-serif'> $msg_excessao </font></div>";
	 }	
   ?>
      </td>
    </tr>
    <tr> 
      <td colspan="2" class="labelCentro"><hr></td>
    </tr>
    <tr> 
      <td colspan="2" class="labelEsquerda"> </td>
    </tr>
    <tr> 
      <td colspan="2" class="labelEsquerda"><strong> Atendimento sa&uacute;de</strong></td>
    </tr>
    <tr> 
      <td colspan="2" class="labelEsquerda">&nbsp;</td>
    </tr>
    <tr> 
      <td width="28%" class="labelDireita">Nome:</td>
      <td class="labelEsquerda"><?php echo $nm_associado;?></td>
    </tr>
    <tr> 
      <td class="labelDireita">Sexo</td>
      <td class="labelEsquerda"><?php echo $nm_sexo;?></td>
    </tr>
    <tr> 
      <td class="labelDireita">RG:</td>
      <td class="labelEsquerda"><?php echo $nu_rg;?></td>
    </tr>
    <tr> 
      <td class="labelDireita">Data In&iacute;cio:</td>
      <td class="labelEsquerda"><input type="text" name="dt_inicio"  value="<?php echo $dt_inicio;?>" size="12" maxlength="10" onKeyUp="barra(this)"></td>
    </tr>
    <tr> 
      <td class="labelDireita">Data Fim:</td>
      <td class="labelEsquerda"><input type="text" name="dt_fim"  value="<?php echo $dt_fim;?>" size="12" maxlength="10" onKeyUp="barra(this)"></td>
    </tr>
    <tr> 
      <td class="labelDireita">Hora:</td>
      <td class="labelEsquerda"><input type="text" name="hr_psico"  value="<?php echo $hr_psico;?>" size="6" maxlength="5" onKeyUp="barra_hora(this)"></td>
    </tr>
    <?
	//CONECTA AO MYSQL              
	    require_once("class/conexao.php");
		$mysql = new Mysql();
		$mysql->conectar(); 
		
		$sql_cate = "SELECT * FROM categorias ORDER BY nm_categoria"; 
		$sql_cate = mysql_query($sql_cate);       
		$row_cate = mysql_num_rows($sql_cate);
	?>
    <tr> 
      <td class="labelDireita">Categoria:</td>
      <td class="labelEsquerda"><select name="id_categoria" id="id_categoria" onChange="combo_categoria()" >
          <option value="<?php echo $id_categoria ? $id_categoria : "0";?>"><?php echo $nm_categoria ? $nm_categoria : "-- Selecione -- >>";?></option>
          <?php for($a=0; $a<$row_cate; $a++) { ?>
          <option value="<?php echo mysql_result($sql_cate, $a, "id_categoria"); ?>"> 
          <?php echo mysql_result($sql_cate, $a, "nm_categoria"); ?></option>
          <?php } ?>
        </select></td>
    </tr>
    <?
	  	   		
		$sql_psico = "SELECT * FROM psicanalistas ORDER BY nm_psicanalista"; 
		$sql_psico = mysql_query($sql_psico);       
		$row_psico = mysql_num_rows($sql_psico);
	?>
    <tr> 
      <td class="labelDireita">Profissional:</td>
      <td class="labelEsquerda"><select name="id_psicanalista" id="id_psicanalista" onChange="combo_psico()" >
          <option value="<?php echo $id_psicanalista ? $id_psicanalista : "0";?>"><?php echo $nm_psicanalista ? $nm_psicanalista : "-- Selecione -- >>";?></option>
          <?php for($a=0; $a<$row_psico; $a++) { ?>
          <option value="<?php echo mysql_result($sql_psico, $a, "id_psicanalista"); ?>"> 
          <?php echo mysql_result($sql_psico, $a, "nm_psicanalista"); ?></option>
          <?php } ?>
        </select></td>
    </tr>
    <tr> 
      <td class="labelDireita">Observa&ccedil;&atilde;o:</td>
      <td class="labelEsquerda"><input type="text" name="te_obs"  value="<?php echo $te_obs;?>" size="70" maxlength="90" ></td>
    </tr>
    <tr> 
      <td colspan="2" class="labelEsquerda"><hr></td>
    </tr>
    <tr> 
      <td colspan="2" class="labelEsquerda"><input type="hidden" name="id_associado" value="<?php echo $id_associado;?>"> 
        <input type="hidden" name="nm_associado" value="<?php echo $nm_associado;?>"> 
        <input type="hidden" name="nm_psicanalista" id="nm_psicanalista" value="<?php echo $nm_psicanalista;?>"> 
        <input type="hidden" name="nm_categoria" id="nm_categoria" value="<?php echo $nm_categoria;?>"></td>
    </tr>
    <tr> 
      <td colspan="6" class="labelCentro"><input name="inserirPsico" type="submit" value="Gravar" /> 
        <input name="volta" type="button" id="volta" value="Cancelar" onClick="javascript:executar('controller/cancela_controller.php?cancelarOperacao=cancelarOperacao')" /> 
      </td>
    </tr>
  </table>
</form> 
<?php } else {?>
    
<table width="53%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td colspan="8" class="labelEsquerda">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="8" class="labelEsquerda">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="8" class="labelEsquerda">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" class="labelEsquerda"><strong> Atendimento sa&uacute;de</strong></td>
  </tr>
  <tr> 
    <td colspan="8" class="labelEsquerda">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="8" class="labelEsquerda"> 
      <?php
  if ($msg_sucesso){
   echo "<div align='left'>
   <img src='img/msg_azul.png' width='20' height='20'><font color='#0099CC' size='2' face='Arial, Helvetica, sans-serif'> $msg_sucesso </font></div>";
     } else if ($msg_excessao){
	   echo "<div align='left'>
   <img src='img/msg_vermelha.gif' width='20' height='20'><font color='#FF0000' size='2' face='Arial, Helvetica, sans-serif'> $msg_excessao </font></div>";
	 }	
   ?>
    </td>
  </tr>
  <tr> 
    <td colspan="8" class="labelEsquerda">&nbsp;</td>
  </tr>
  <tr bgcolor="#999999"> 
    <td width="25%" class="labelEsquerda"><font color="#FFFFFF"><strong>Associado</strong>&nbsp;(a)&nbsp;</font></td>
    <td width="10%" class="labelEsquerda"><font color="#FFFFFF"><strong>Data In&iacute;cio</strong></font></td>
    <td width="9%" class="labelEsquerda"><font color="#FFFFFF"><strong>Data Fim</strong></font></td>
    <td width="6%" class="labelEsquerda"><font color="#FFFFFF"><strong>Hora</strong></font></td>
    <td width="25%" class="labelEsquerda"><font color="#FFFFFF"><strong>Profissional</strong></font></td>
    <td width="8%" class="labelCentro"><font color="#FFFFFF"><strong>Inserir</strong></font></td>
    <td width="7%" class="labelCentro"><font color="#FFFFFF"><strong>Editar</strong></font></td>
    <td width="10%" class="labelCentro"><strong><font color="#FFFFFF">Excluir</font></strong><strong><font color="#FFFFFF">&nbsp;</font></strong></td>
  </tr>
  <?php
 	require_once("class/conexao.php");
	 //CONECTA AO MYSQL              
	$mysql = new Mysql();
	$mysql->conectar(); 
	
	 $sql = mysql_query("SELECT * FROM psicos, psicanalistas WHERE psicos.id_psicanalista = psicanalistas.id_psicanalista AND id_psico = ".$id_psico." 	");
    $row = mysql_num_rows($sql);
	for ( $i=0; $i < $row; $i++ ){
	
 	$dt_inicio = mysql_result($sql, $i, "dt_inicio");
	$dt_inicio = substr($dt_inicio,8,2)."/".substr($dt_inicio,5,2)."/".substr($dt_inicio,0,4);
	$dt_fim = mysql_result($sql, $i, "dt_fim");
	$dt_fim = substr($dt_fim,8,2)."/".substr($dt_fim,5,2)."/".substr($dt_fim,0,4);
	$hr_psico = mysql_result($sql, $i, "hr_psico");
	$nm_psicanalista = mysql_result($sql, $i, "nm_psicanalista");
	$id_usuario = mysql_result($sql, $i, "id_usuario");
?>
  <tr> 
    <td class="labelEsquerda"><?php echo $nm_associado;?></td>
    <td class="labelEsquerda"><?php echo $dt_inicio;?></td>
    <td class="labelEsquerda"><?php echo $dt_fim;?></td>
    <td class="labelEsquerda"><?php echo $hr_psico;?></td>
    <td class="labelEsquerda"><?php echo $nm_psicanalista;?></td>
    <td class="labelCentro"><a href="controller/associado_controller.php?abrirAssociadoLista=abrirAssociadoLista"><img src="img/insert.gif" width="10" height="10"></a></td>
    <td class="labelCentro"><a href="controller/psico_controller.php?abrirPsicoEdit=abrirPsicoEdit&id_psico=<?php echo $id_psico;?>&opcao=2&id_usuario=<?php echo $id_usuario;?>"><img src="img/insert.gif" width="10" height="10"></a></td>
    <td class="labelCentro"><a href="javascript:del('controller/psico_controller.php?excluirAula=excluirAula&id_psico=<?php echo $id_psico;?>&opcao=1')"><img src="img/excluir2.gif" width="10" height="10"></a></td>
  </tr>
  <?php }?>
  <tr> 
    <td colspan="11" class="labelEsquerda"><hr></td>
  </tr>
  <tr> 
    <td colspan="8" class="labelCentro"> <input name="volta" type="button" id="volta" value="Cancelar" onClick="javascript:executar('controller/cancela_controller.php?cancelarOperacao=cancelarOperacao')" /> 
    </td>
  </tr>
</table>
  <?php }?></body>

<?php
} else {
 include "rodape/rodape.php";
 }
 ?>
</html>
