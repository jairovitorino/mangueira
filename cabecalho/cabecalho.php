<?php
@session_start();

$titulo = @$_SESSION['titulo'];
$titulo == "" ? $titulo = "Projeto Mangueira" : $titulo = $titulo;
$icon = @$_SESSION['icon'];
$icon == "" ? $icon = "img/mansion.png" : $icon = $icon;
// strtoupper();

if (isset($_SESSION['login_mangueira'])){
$login_mangueira = $_SESSION['login_mangueira'];
}
if (isset($_SESSION['acesso'])){
$acesso = $_SESSION['acesso'];
}
if (isset($_SESSION['id_status'])){
$id_status = $_SESSION['id_status'];
}
if (isset($_SESSION['msg'])){
$msg = $_SESSION['msg'];
}

?>
<html>
<head>
<title><?php echo $titulo;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
function imprimir(detUrl){
document.location = detUrl;
}
</script>
</head>
<link rel="stylesheet" href="css/textos.css" type="text/css">
<link rel="stylesheet" href="css/estilo.css" type="text/css">
<link rel="stylesheet" href="css/filadelfia.css" type="text/css">
<link rel="SHORTCUT ICON" href="<?php echo $icon;?>"/>
<body bgcolor="#F5F5F5" onLoad="javascript:caixas()">
<form name="form1" action="controller/usuario_controller.php" method="post">

<?php if ( @$acesso == 1 ){?>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr bgcolor="#FFFFFF"> 
    <td colspan="5"><font color="#000099" size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
  </tr>
 <tr bgcolor="#333333">  
      <td colspan="5"><div align="center"><font color="#000099" size="2" face="Arial, Helvetica, sans-serif">&nbsp;<font color="#FFFFFF">PROJETO 
          MANGUEIRA </font></font></div></td>
  </tr>
   <tr bgcolor="#ffffff"> 
    <td colspan="5"><div align="center"><font color="#000099" size="2" face="Arial, Helvetica, sans-serif">&nbsp;<font color="#FFFFFF">ADMINISTRA&Ccedil;&Atilde;O</font></font></div></td>
  </tr>
  <script type="text/javascript" src="js/menu.js"></script>
  <tr> 
    <td width="18%" bordercolor="#FFFFFF" bgcolor="#FFFFFF" class="labelCentro"><font color="#FF0000" size="2" face="Courier New, Courier, mono">&nbsp; 
      </font></td>
      <td><ul class="udm" id="udm" name="udm"> <li><a href="#">Cadastros</a> 
          <ul>
            <?php if ( ($login_mangueira != 'admin')){?>
           	<li><a href="controller/associado_controller.php?abrirAssociadoLookup=abrirAssociadoLookup">Associado</a> </li>
			<li><a href="controller/ponto_controller.php?abrirPontoEletronico=abrirPontoEletronico">Ponto Eletronico</a></li>
			  <?php } else {?>
          			<li><a href="#">Associado</a> </li>
					<li><a href="#">Ponto Eletronico</a> </li>
		  <?php }?>
          </ul>
        <li><a href="#">Consultas</a> 
	     <ul>
         		  	  
		   <li><a href="#">Associados</a> 
            <ul>
              <li><a href="controller/associado_controller.php?abrirAssociadoLista=abrirAssociadoLista">Todos</a></li>
              <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=nome_associado">Nome</a></li>
			    <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=logra_associado">Logradouro</a></li>
				<li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=bairro_associado">Bairro</a></li>
              <ul>
              </ul>		  
            </ul>
          </li>
		  
		    <li><a href="#">Bloco Mangueirinha</a> 
              <ul>
              <li><a href="controller/bloco_controller.php?abrirBlocoLista=abrirBlocoLista">Todos</a></li>
                <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=foliao_bloco">Foli&atilde;o</a></li>
			               <ul>
              </ul>
            </ul>
          </li>
		  
		    <li><a href="#">Curriculos</a> 
            <ul>
              <li><a href="controller/curriculo_controller.php?abrirCurriculoLista=abrirCurriculoLista">Todos</a></li>
              <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=nome_curriculo">Nome</a></li>
			    <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=profissao_curriculo">Profiss&atilde;o</a></li>
				  <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=indicacao_curriculo">Indica&ccedil;&atilde;o</a></li>
			  <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=logra_curriculo">Logradouro</a></li>
			  <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=bairro_curriculo">Bairro</a></li>
			  <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=idade_curriculo">Idade</a></li>
              <ul>
              </ul>
            </ul>
          </li>
		  
		    <li><a href="#">Atendimentos sa&uacute;de</a> 
              <ul>
              <li><a href="controller/psico_controller.php?abrirPsicoLista=abrirPsicoLista">Todos</a></li>
              <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=nome_psico">Associado</a></li>
			    
              <ul>
              </ul>		  
            </ul>
          </li>		 
		  
		  <li><a href="#">Treinamentos</a> 
            <ul>
              <li><a href="controller/treino_controller.php?abrirTreinoLista=abrirTreinoLista">Todos</a></li>
              <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=nome_treino">Associado</a></li>
			    
              <ul>
              </ul>		  
            </ul>
          </li>		 
           
		    <li><a href="#">Solicita&ccedil;&otilde;es</a> 
              <ul>
              <li><a href="controller/solicitacao_controller.php?abrirSolicitacaoLista=abrirSolicitacaoLista">Todos</a></li>
              <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=nome_solicitacao">Associado</a></li>
			    <li><a href="controller/pesquisa_controller.php?abrirPesquisaObjeto=abrirPesquisaObjeto&opcao=nome_servico">Servi&ccedil;o</a></li>
              <ul>
              </ul>		  
            </ul>
          </li>		 
          		  
			<li><a href="controller/associado_controller.php?abrirAniversarianteLista=abrirAniversarianteLista">Aniversariantes</a></li>
			<li><a href="controller/painel_controller.php?abrirPainel=abrirPainel">Painel</a></li>	
		    <li><a href="controller/ponto_controller.php?abrirPontoManual=abrirPontoManual">Ponto Manual</a></li>
			
            </ul>
      		
		 <li><a href="#">Manuten&ccedil;&atilde;o</a> 
          <ul> 
				
			<li><a href="controller/usuario_controller.php?abrirUsuarioLista=abrirUsuarioLista">Usu&aacute;rios</a></li>
				 <?php if ( ($login_mangueira == 'admin') || ($id_status == 1 ) || ($_SESSION['nm_login'] == 'amandashallin@hotmail.com' ) ){?>
				<li><a href="#">Dados</a>
					<ul>
						<li><a href="#">Curso</a>
							<ul>
								<li><a href="controller/treino_controller.php?abrirCurso=abrirCurso">Inserir</a></li>
								<li><a href="controller/treino_controller.php?abrirCursoLista=abrirCursoLista">Listar</a></li>
							</ul>							
						</li>
						
						<li><a href="#">Categoria</a>
							<ul>
								<li><a href="controller/psico_controller.php?abrirCategoria=abrirCategoria">Inserir</a></li>
								<li><a href="controller/psico_controller.php?abrirCategoriaLista=abrirCategoriaLista">Listar</a></li>
							</ul>							
						</li>
						
						<li><a href="#">Profissional</a>
							<ul>
								<li><a href="controller/psico_controller.php?abrirPsicanaLookup=abrirPsicanaLookup">Inserir</a></li>
								<li><a href="controller/psico_controller.php?abrirPsicanalistaLista=abrirPsicanalistaLista">Listar</a></li>
							</ul>							
						</li>
						
						<li><a href="#">Servi&ccedil;o</a>
							<ul>
								<li><a href="controller/solicitacao_controller.php?abrirServico=abrirServico">Inserir</a></li>
								<li><a href="controller/solicitacao_controller.php?abrirServicoLista=abrirServicoLista">Listar</a></li>
							</ul>														
						</li>
						
																							
					</ul>					
								
				</li>	
				
					<li><a href="#">Backup</a>
							<ul>
								<li><a href="controller/usuario_controller.php?copiarDados=copiarDados">Executar</a></li>
		 		 				<li><a href="controller/usuario_controller.php?abrirBackup=abrirBackup">Exibir</a></li>
								</ul>
								
								<li><a href="controller/usuario_controller.php?abrirLogLista=abrirLogLista">Log</a></li>
								
							<?php }?>													
						</li>										
						
						
			</ul>				
		
			 <li><a href="#">Ajuda</a>
		 <ul> 
			
			<li><a href="controller/usuario_controller.php?abrirInformacoes=abrirInformacoes">Informa&ccedil;&otilde;es</a></li>
			<li><a href="#">Contrato</a>
				<ul>
					<li><a href="controller/usuario_controller.php?imprimirContratoOnline=imprimirContratoOnline">onLine</a></li>
					<li><a href="controller/usuario_controller.php?imprimirContrato=imprimirContrato">Padrao</a></li>
				</ul>
			</li>
			<li><a href="#" title="28/11/2018 as 15:35">Release 1.0</a></li>  
				
			  <?php if ( ($_SESSION['login_mangueira'] == 'jairo.vitorino@gmail.com') ){?>	
			<li><a href="controller/usuario_controller.php?abrirBoasVindas=abrirBoasVindas">Boas Vindas</a></li>
			<li><a href="controller/usuario_controller.php?abrirLogLista=abrirLogLista">Log</a></li>
			    			 
		   </ul>		  			
		<?php }?>			
			</ul>
	  
	  </td>
      <td width="31%" bordercolor="#FFFFFF" bgcolor="#FFFFFF" class="labelCentro"> 
        Usu&aacute;rio <?php echo $_SESSION['nm_status']?>:&nbsp;<font color="#ff0000"><?php echo @$login_mangueira;?>:</font>&nbsp;&nbsp;(<a href="controller/usuario_controller.php?logout=logout">Sair</a>)</td>
      <td width="13%" bordercolor="#FFFFFF" bgcolor="#FFFFFF" class="labelCentro">&nbsp;</td>
    
  </tr>
</table>
<?php } else {?>
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr bgcolor="#FFFFFF"> 
    <td colspan="5"><font color="#000099" size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
  </tr>
   <tr bgcolor="#333333"> 
    <td colspan="5"><div align="center"><font color="#000099" size="2" face="Arial, Helvetica, sans-serif"><font color="#FFFFFF">PROJETO 
          MANGUEIRA</font></font></div></td>
  </tr>
     <tr bgcolor="#ffffff"> 
    <td colspan="5"><div align="center"><font color="#000099" size="2" face="Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp;<font color="#FFFFFF">ADMINISTRA&Ccedil;&Atilde;O</font></font></div></td>
  </tr>
    <tr> 
      <td width="18%" bordercolor="#FFFFFF" bgcolor="#FFFFFF" class="labelCentro">&nbsp;</td>
      <td width="38%" bordercolor="#FFFFFF" background="../calendario.php" bgcolor="#FFFFFF" class="labelCentro">&nbsp;</td>
      <td width="32%" bordercolor="#FFFFFF" bgcolor="#FFFFFF" class="labelCentro"> 
        E-mail 
        <input name="login" type="text" size="25" maxlength="50">
        Senha
        <input name="senha" type="password" size="8" maxlength="50">
        <input type="submit" value="Ok">
       
        &nbsp;<font color="#ff0000"><?php echo @$msg;?></font>
		
        <input type="hidden" name="logar"></td>
      <td width="12%" bordercolor="#FFFFFF" bgcolor="#FFFFFF" class="labelCentro"><a href="controller/usuario_controller.php?novoUsuario=novoUsuario">Criar Conta</a></td>
    </tr>
  </table>
<?php }?>
</form>
</body>

</html>
