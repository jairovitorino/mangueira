<?php
session_start();

//CONECTA AO MYSQL              
require_once("class/conexao.php");
$mysql = new Mysql();
$mysql->conectar(); 


define("FPDF_FONTPATH","fpdf/font/");
require_once("fpdf/fpdf.php");
$pdf = new FPDF('P'); 
$pdf->Open(); 

$pdf->AddPage(); 

$pdf->Image('img/logo_projeto.jpg',96,9,25,20);

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(92, 33);
$texto = "PROJETO MANGUEIRA";
$pdf->Cell(0,0.5,$texto, 4, 'J');

// 1. Retangulo
$pdf->Rect(18, 54, 180, 18 , "D");

$nu_ano = date("Y");
$id_associado = $_SESSION['id_associado'];

$sql = mysql_query("SELECT * FROM associados WHERE id_associado = ".$id_associado." ");
$row = mysql_num_rows($sql);
for ($i=0;$i < $row ; $i++){
	$nm_associado = mysql_result($sql, $i, "nm_associado");
	$id_sexo = mysql_result($sql, $i, "id_sexo");
	$id_sexo == 1 ? $nm_sexo = "M" : $nm_sexo = "F";
	$nu_rg = mysql_result($sql, $i, "nu_rg");
	$dt_nascimento = mysql_result($sql, $i, "dt_nascimento");
	$dt_nascimento = substr($dt_nascimento,8,2)."/".substr($dt_nascimento,5,2)."/".substr($dt_nascimento,0,4);
	$nu_telefone = mysql_result($sql, $i, "nu_telefone");
	$nu_whatsapp = mysql_result($sql, $i, "nu_whatsapp");
	$nm_logra = mysql_result($sql, $i, "nm_logra");
	$nu_logra = mysql_result($sql, $i, "nu_logra");		
	$nm_bairro = mysql_result($sql, $i, "nm_bairro");
	$nu_cep = mysql_result($sql, $i, "nu_cep");
	$nm_cidade = mysql_result($sql, $i, "nm_cidade");
	$nm_uf = mysql_result($sql, $i, "nm_uf");
	$te_imagem = mysql_result($sql, $i, "te_imagem");	
	$te_imagem = substr($te_imagem,14);
	}


// Retangulo da foto
$pdf->Rect(101, 36, 15, 15 , "D");
if ( $te_imagem != ""){
	$te_imagem = $te_imagem;
	$pdf->Image('fotos/'.$te_imagem,102,37,13,13);
	} else if ( $te_imagem == "" && $id_sexo == 1){
	$te_imagem = "img_masc.jpg";
	$pdf->Image('img/'.$te_imagem,102,37,13,13);
	} else {
	$te_imagem = "img_fem.jpg";
	$pdf->Image('img/'.$te_imagem,102,37,13,13);
	}

 
$pdf->SetFont('Arial', '', 6);
$pdf->SetXY(18, 52);
$texto = "DADOS DO ASSOCIADO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(20, 58);
$texto = "Nome: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(30, 58);
$texto = $nm_associado;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(78, 58);
$texto = "Sexo: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(87, 58);
$texto = $nm_sexo;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(92, 58);
$texto = "RG: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(98, 58);
$texto = $nu_rg;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(115, 58);
$texto = "Nascimento: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(133, 58);
$texto = $dt_nascimento;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(148, 58);
$texto = "Telefone: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(162, 58);
$texto = $nu_telefone;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(20, 63);
$texto = "Whatsapp: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(36, 63);
$texto = $nu_whatsapp;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(56, 63);
$texto = "Rua/Av.: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(69, 63);
$texto = $nm_logra;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(128, 63);
$texto = "N: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(131, 63);
$texto = $nu_logra;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(140, 63);
$texto = "Bairro: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(151, 63);
$texto = $nm_bairro;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(20, 68);
$texto = "CEP: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(29, 68);
$texto = $nu_cep;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(45, 68);
$texto = "Cidade: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(58, 68);
$texto = $nm_cidade;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(88, 68);
$texto = "UF: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(95, 68);
$texto = $nm_uf;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 6);
$pdf->SetXY(18, 77);
$texto = "DADOS DO CURRICULO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

// 2. Retangulo
$pdf->Rect(18, 79, 180, 18 , "D");

$pdf->SetFont('Arial', 'B', 7);
$pdf->SetXY(20, 81);
$pdf->Cell(40,5,'PROFISS�O');
$pdf->SetXY(100, 81);
$pdf->Cell(40,5,'INDICA��O');
$pdf->SetXY(145, 81);
$pdf->Cell(40,5,'SITUA��O');

$sql = mysql_query("SELECT * FROM curriculos WHERE id_associado = ".$id_associado." ");

$pdf->SetFont('Arial', '', 7);
$j = 1;
while ( $vetor = mysql_fetch_array($sql) ){
switch($vetor['st_curriculo']){
	case 1;
	$nm_curriculo = "Cadastrado";
	break;
	case 2;
	$nm_curriculo = "Encaminhado";
	break;
	case 3;
	$nm_curriculo = "Trabalhando";
	break;	
		
	}	
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(0,5,$vetor['nm_profissao']);
$pdf->SetX(100);
$pdf->Cell(0,5,$vetor['nm_indicacao']);
$pdf->SetX(145);
$pdf->Cell(0,5,$nm_curriculo);

$j = $j + 1;
}

$pdf->SetFont('Arial', '', 6);
$pdf->SetXY(18, 102);
$texto = "DADOS DO CURSO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

// 3. Retangulo
$pdf->Rect(18, 104, 180, 28 , "D");

$pdf->SetFont('Arial', 'B', 7);
$pdf->SetXY(20, 106);
$pdf->Cell(40,5,'OR');
$pdf->SetXY(27, 106);
$pdf->Cell(40,5,'CURSO');
$pdf->SetXY(70, 106);
$pdf->Cell(40,5,'CARGA HO.');
$pdf->SetXY(90, 106);
$pdf->Cell(40,5,'IN�CIO');
$pdf->SetXY(110, 106);
$pdf->Cell(40,5,'FIM');
$pdf->SetXY(130, 106);
$pdf->Cell(40,5,'HORA');


$sql = mysql_query("SELECT * FROM treinos, cursos WHERE treinos.id_curso = cursos.id_curso AND id_associado = ".$id_associado." AND YEAR(treinos.dt_fim) = ".$nu_ano." ");

$pdf->SetFont('Arial', '', 7);
$j = 1;
while ( $vetor = mysql_fetch_array($sql) ){
$dt_inicio = substr($vetor['dt_inicio'],8,2)."/".substr($vetor['dt_inicio'],5,2)."/".substr($vetor['dt_inicio'],0,4);
$dt_fim = substr($vetor['dt_fim'],8,2)."/".substr($vetor['dt_fim'],5,2)."/".substr($vetor['dt_fim'],0,4);
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(0,5,$j);
$pdf->SetX(27);
$pdf->Cell(0,5,$vetor['nm_curso']);
$pdf->SetX(70);
$pdf->Cell(0,5,$vetor['nu_carga']);
$pdf->SetX(90);
$pdf->Cell(0,5,$dt_inicio);
$pdf->SetX(110);
$pdf->Cell(0,5,$dt_fim);
$pdf->SetX(130);
$pdf->Cell(0,5,$vetor['hr_treino']);

$j = $j + 1;
}

$pdf->SetFont('Arial', '', 6);
$pdf->SetXY(18, 137);
$texto = "DADOS AT. SA�DE";
$pdf->Cell(0,0.5,$texto, 4, 'J');

// 4. Retangulo
$pdf->Rect(18, 139, 180, 28 , "D");

$pdf->SetFont('Arial', 'B', 7);
$pdf->SetXY(20, 141);
$pdf->Cell(40,5,'OR');
$pdf->SetXY(27, 141);
$pdf->Cell(40,5,'IN�CIO');
$pdf->SetXY(47, 141);
$pdf->Cell(40,5,'FIM');
$pdf->SetXY(67, 141);
$pdf->Cell(40,5,'HORA');
$pdf->SetXY(77, 141);
$pdf->Cell(40,5,'CATEGORIA');
$pdf->SetXY(100, 141);
$pdf->Cell(40,5,'PROFISSIONAL');

$sql = mysql_query("SELECT * FROM psicos, categorias, psicanalistas WHERE psicos.id_categoria = categorias.id_categoria AND psicos.id_psicanalista = psicanalistas.id_psicanalista AND id_associado = ".$id_associado." AND YEAR(psicos.dt_fim) = ".$nu_ano." ");

$pdf->SetFont('Arial', '', 7);
$j = 1;
while ( $vetor = mysql_fetch_array($sql) ){
$dt_inicio = substr($vetor['dt_inicio'],8,2)."/".substr($vetor['dt_inicio'],5,2)."/".substr($vetor['dt_inicio'],0,4);
$dt_fim = substr($vetor['dt_fim'],8,2)."/".substr($vetor['dt_fim'],5,2)."/".substr($vetor['dt_fim'],0,4);
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(0,5,$j);
$pdf->SetX(27);
$pdf->Cell(0,5,$dt_inicio);
$pdf->SetX(47);
$pdf->Cell(0,5,$dt_fim);
$pdf->SetX(67);
$pdf->Cell(0,5,$vetor['hr_psico']);
$pdf->SetX(77);
$pdf->Cell(0,5,$vetor['nm_categoria']);
$pdf->SetX(100);
$pdf->Cell(0,5,$vetor['nm_psicanalista']);


$j = $j + 1;
}

$pdf->SetFont('Arial', '', 6);
$pdf->SetXY(18, 172);
$texto = "DADOS BLOCO INFANTIL";
$pdf->Cell(0,0.5,$texto, 4, 'J');

// 5. Retangulo
$pdf->Rect(18, 174, 180, 28 , "D");

$pdf->SetFont('Arial', 'B', 7);
$pdf->SetXY(20, 177);
$pdf->Cell(40,5,'OR');
$pdf->SetXY(27, 177);
$pdf->Cell(40,5,'FOLI�O');
$pdf->SetXY(87, 177);
$pdf->Cell(40,5,'IDADE');
$pdf->SetXY(107, 177);
$pdf->Cell(40,5,'PARENTESCO');
$pdf->SetXY(130, 177);
$pdf->Cell(40,5,'RELA��O');
$pdf->SetXY(160, 177);
$pdf->Cell(40,5,'ANO');

$nu_ano_atual = $nu_ano;
$nu_ano_prox = $nu_ano +1;

$sql = mysql_query("SELECT blocos.dt_nascimento,(YEAR(CURDATE())-YEAR(blocos.dt_nascimento)) AS nu_idade,blocos.nm_foliao,blocos.st_responsavel,blocos.st_parentesco,blocos.id_associado,nu_rg_foliao,nu_ano 
FROM blocos  
WHERE blocos.id_associado = ".$id_associado." AND nu_ano = ".$nu_ano_atual."  ");

$pdf->SetFont('Arial', '', 7);
$j = 1;
while ( $vetor = mysql_fetch_array($sql) ){
switch($vetor['st_responsavel']){
	case 1;
	$st_responsavel = "M�e";
	break;
	case 2;
	$st_responsavel = "Pai";
	break;
	case 3;
	$st_responsavel = "Outro";
	break;		
	}	
	
	switch($vetor['st_parentesco']){
	case 1;
	$st_parentesco = "Filho";
	break;
	case 2;
	$st_parentesco = "Outro";
	break;
	
	}	$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(0,5,$j);
$pdf->SetX(27);
$pdf->Cell(0,5,$vetor['nm_foliao']);
$pdf->SetX(87);
$pdf->Cell(0,5,$vetor['nu_idade']);
$pdf->SetX(107);
$pdf->Cell(0,5,$st_parentesco);
$pdf->SetX(130);
$pdf->Cell(0,5,$st_responsavel);
$pdf->SetX(160);
$pdf->Cell(0,5,$vetor['nu_ano']);


$j = $j + 1;
}

$pdf->SetFont('Arial', '', 6);
$pdf->SetXY(18, 207);
$texto = "DADOS SOLICITA��ES";
$pdf->Cell(0,0.5,$texto, 4, 'J');

// 6. Retangulo
$pdf->Rect(18, 209, 180, 28 , "D");

$pdf->SetFont('Arial', 'B', 7);
$pdf->SetXY(20, 211);
$pdf->Cell(40,5,'OR');
$pdf->SetXY(27, 211);
$pdf->Cell(40,5,'SOLICITANTE');
$pdf->SetXY(67, 211);
$pdf->Cell(40,5,'SERVI�O');
$pdf->SetXY(115, 211);
$pdf->Cell(40,5,'DATA');


$sql = mysql_query("SELECT * FROM 
solicitacoes, servicos 
WHERE id_associado = ".$id_associado." 
AND solicitacoes.id_servico = servicos.id_servico 
AND nu_ano = ".$nu_ano."");

$pdf->SetFont('Arial', '', 7);
$j = 1;
while ( $vetor = mysql_fetch_array($sql) ){
$dt_solicitacao = substr($vetor['dt_solicitacao'],8,2)."/".substr($vetor['dt_solicitacao'],5,2)."/".substr($vetor['dt_solicitacao'],0,4);
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(0,5,$j);
$pdf->SetX(27);
$pdf->Cell(0,5,$vetor['nm_solicitante']);
$pdf->SetX(67);
$pdf->Cell(0,5,$vetor['nm_servico']);
$pdf->SetX(115);
$pdf->Cell(0,5,$dt_solicitacao);

}

$pdf->Output();
?>