-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: mysql.lauraware.com.br    Database: lauraware06
-- ------------------------------------------------------
-- Server version	5.5.40-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `lauraware06`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `lauraware06` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `lauraware06`;

--
-- Table structure for table `associados`
--

DROP TABLE IF EXISTS `associados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `associados` (
  `id_associado` int(11) NOT NULL AUTO_INCREMENT,
  `nm_associado` varchar(30) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `id_escolaridade` int(11) NOT NULL,
  `nu_rg` varchar(10) NOT NULL,
  `dt_nascimento` date NOT NULL,
  `nu_telefone` varchar(30) NOT NULL,
  `nm_logra` varchar(40) NOT NULL,
  `nu_logra` varchar(6) NOT NULL,
  `nm_bairro` varchar(20) NOT NULL,
  `nu_cep` varchar(8) NOT NULL,
  `nm_cidade` varchar(20) NOT NULL,
  `nm_uf` varchar(2) NOT NULL,
  `te_obs` varchar(250) NOT NULL,
  `te_imagem` varchar(30) NOT NULL,
  `st_controle` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_associado`),
  KEY `id_escolaridade` (`id_escolaridade`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `associados`
--

LOCK TABLES `associados` WRITE;
/*!40000 ALTER TABLE `associados` DISABLE KEYS */;
INSERT INTO `associados` VALUES (14,'CARLOS SANTOS ALMEIDA',1,0,'7215849795','1985-12-15','33264589','RUA MARIO ARANTES','15','SAO CAETANO','40325654','SALVADOR','BA','CURSO','Boleto Amanda.pdf',0,57);
/*!40000 ALTER TABLE `associados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculos`
--

DROP TABLE IF EXISTS `curriculos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curriculos` (
  `id_curriculo` int(11) NOT NULL AUTO_INCREMENT,
  `nm_curriculo` varchar(30) NOT NULL,
  `nu_cpf` varchar(11) NOT NULL,
  `te_destino` varchar(30) NOT NULL,
  `nm_profissao` varchar(30) NOT NULL,
  `nm_logra` varchar(40) NOT NULL,
  `nu_logra` varchar(4) NOT NULL,
  `nm_bairro` varchar(20) NOT NULL,
  `nu_cep` varchar(8) NOT NULL,
  `nm_cidade` varchar(20) NOT NULL,
  `nm_uf` varchar(2) NOT NULL,
  `dt_destino` date NOT NULL,
  `te_obs` varchar(255) NOT NULL,
  `te_imagem` varchar(50) NOT NULL,
  `st_controle` int(11) NOT NULL DEFAULT '1',
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_curriculo`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculos`
--

LOCK TABLES `curriculos` WRITE;
/*!40000 ALTER TABLE `curriculos` DISABLE KEYS */;
INSERT INTO `curriculos` VALUES (54,'JULIA ROMANA DE MENEZES','','','DOCEIRA, COZINHEIRA','AV PEIXE','44','ONDINA','41320480','SALVADOR','BA','0000-00-00','','nailson_luciano.pdf',1,41);
/*!40000 ALTER TABLE `curriculos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `dt_log` date NOT NULL,
  `hr_log` varchar(5) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_acao` int(11) NOT NULL,
  `nm_objeto` varchar(50) NOT NULL,
  `id_indice` int(11) NOT NULL,
  `nu_ip` varchar(50) NOT NULL,
  `nm_acesso` varchar(50) NOT NULL,
  PRIMARY KEY (`id_log`),
  KEY `id_usuario` (`id_usuario`,`id_acao`),
  KEY `id_indice` (`id_indice`)
) ENGINE=MyISAM AUTO_INCREMENT=2636 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (2635,'2018-11-06','22:47',41,5,'Login',41,'170.79.21.18',''),(2634,'2018-11-06','22:40',41,5,'Login',41,'170.79.21.18','');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psicanalistas`
--

DROP TABLE IF EXISTS `psicanalistas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psicanalistas` (
  `id_psicanalista` int(11) NOT NULL AUTO_INCREMENT,
  `nm_psicanalista` varchar(30) NOT NULL,
  `te_conselho` varchar(8) NOT NULL,
  PRIMARY KEY (`id_psicanalista`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psicanalistas`
--

LOCK TABLES `psicanalistas` WRITE;
/*!40000 ALTER TABLE `psicanalistas` DISABLE KEYS */;
INSERT INTO `psicanalistas` VALUES (3,'DRA ALZIRA MOREIRA CALAZANS','7875/BA'),(4,'DR WILLIAM AZEVEDO COSTA','3427/RJ');
/*!40000 ALTER TABLE `psicanalistas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psicos`
--

DROP TABLE IF EXISTS `psicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psicos` (
  `id_psico` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `dt_inicio` date NOT NULL,
  `dt_fim` date NOT NULL,
  `hr_psico` varchar(5) NOT NULL,
  `nm_psico` varchar(30) NOT NULL,
  `id_psicanalista` int(11) NOT NULL,
  `te_obs` varchar(90) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_psico`),
  KEY `id_associado` (`id_associado`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_psicanalista` (`id_psicanalista`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psicos`
--

LOCK TABLES `psicos` WRITE;
/*!40000 ALTER TABLE `psicos` DISABLE KEYS */;
INSERT INTO `psicos` VALUES (21,14,'2017-11-03','2019-01-03','08:00','',4,'',57),(22,14,'2019-02-02','2019-02-05','08:00','',3,'',57),(23,14,'2019-10-09','2019-10-10','10:00','',4,'',57);
/*!40000 ALTER TABLE `psicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `nm_status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'supervisor'),(2,'operador'),(3,'visitante'),(4,'desligado'),(5,'novo');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nm_usuario` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nm_login` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nm_senha` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `nu_ip` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `id_sexo` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `nu_matricula` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `nu_telefone` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '3',
  `dt_cadastro` date NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_status` (`id_status`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (40,'Administrador','admin','*2AF5EFAC0B14C5132BCD3D297954B53E634D160F','','','','',1,'2017-11-04'),(41,'jairo vitorino','jairo.vitorino@gmail.com','*CAC926C90985FC48783145FD428E3F0EBDCF43A5','::1','','','',1,'2017-11-04'),(56,'hhhhh','hhhh@yyyy.com','*23AE809DDACAF96AF0FD78ED04B6A265E05AA257','170.79.20.111','','','',1,'2018-11-01'),(57,'Amanda Dias','amandashallin@hotmail.com','*A4B6157319038724E3560894F7F932C8886EBFCF','179.183.123.81','','','',1,'2018-11-01');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-06 23:57:09
