<?php 
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();

	
if ( isset($_GET['abrirSolicitacao']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_solicitacao']);
		unset($_SESSION['id_servico']);
		unset($_SESSION['id_curriculo']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nu_cpf']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['dt_nascimento']);
		unset($_SESSION['id_servico']);
		unset($_SESSION['nm_servico']);
		unset($_SESSION['nm_solicitante']);
				
		unset($_SESSION['nm_logra']);
		unset($_SESSION['nu_logra']);				
		unset($_SESSION['te_email']);
		unset($_SESSION['nm_compl']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cep']);
		unset($_SESSION['nm_cidade']);
		unset($_SESSION['nm_uf']);
		unset($_SESSION['opcao']);
		unset($_SESSION['te_obs']);
		
		unset($_SESSION['nm_logra_sol']);
		unset($_SESSION['nu_logra_sol']);				
		unset($_SESSION['nm_bairro_sol']);
		unset($_SESSION['nu_cep_sol']);
		unset($_SESSION['nm_cidade_sol']);
		unset($_SESSION['nm_uf_sol']);
		unset($_SESSION['te_obs_sol']);
		
		
		$id_associado = addslashes($_GET['id_associado']);		
		
		$persistence->abrirSolicitacao($id_associado);
		//header ("location: ../curriculo.php");
	}
	
if (isset($_POST['inserirSolicitacao'])){
	
	unset($_SESSION['msg_sucesso']);
	unset($_SESSION['msg_excessao']);
	
	$id_associado = addslashes($_POST['id_associado']);
	$nm_associado = addslashes($_POST['nm_associado']);
	$id_servico = addslashes($_POST['id_servico']);
	$nm_servico = addslashes($_POST['nm_servico']);
	$nm_solicitante = trim(addslashes($_POST['nm_solicitante']));
	$nm_solicitante = strtoupper($nm_solicitante);
	$nu_telefone_sol = trim(addslashes($_POST['nu_telefone_sol']));
	$dt_solicitacao = addslashes($_POST['dt_solicitacao']);
	$nm_logra_sol = trim(addslashes($_POST['nm_logra_sol']));
	$nm_logra_sol = strtoupper($nm_logra_sol);
	$nu_logra_sol = trim(addslashes($_POST['nu_logra_sol']));
	$nm_bairro_sol = trim(addslashes($_POST['nm_bairro_sol']));
	$nm_bairro_sol = strtoupper($nm_bairro_sol);
	$nu_cep_sol = addslashes($_POST['nu_cep_sol']);			
	$nm_cidade_sol = trim(addslashes($_POST['nm_cidade_sol']));
	$nm_cidade_sol = strtoupper($nm_cidade_sol);
	$nm_uf_sol = addslashes($_POST['nm_uf_sol']);
	$te_obs_sol = addslashes($_POST['te_obs_sol']);
	
	if ( $id_servico == 0 ){
		$msg_excessao = "Servi�o: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['id_servico'] = $id_servico;
		$_SESSION['nm_servico'] = $nm_servico;
		$_SESSION['nm_solicitante'] = $nm_solicitante;
		$_SESSION['nu_telefone_sol'] = $nu_telefone_sol;
		$_SESSION['dt_solicitacao'] = $dt_solicitacao;
		$_SESSION['nm_logra_sol'] = $nm_logra_sol;
		$_SESSION['nu_logra_sol'] = $nu_logra_sol;
		$_SESSION['nm_bairro_sol'] = $nm_bairro_sol;
		$_SESSION['nu_cep_sol'] = $nu_cep_sol;
		$_SESSION['nm_cidade_sol'] = $nm_cidade_sol;
		$_SESSION['nm_uf_sol'] = $nm_uf_sol;
		$_SESSION['te_obs_sol'] = $te_obs_sol;
		header ("location: ../solicitacao.php");
		
	} else if ( $nm_solicitante == "" ){
		$msg_excessao = "Solicitante: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['id_servico'] = $id_servico;
		$_SESSION['nm_servico'] = $nm_servico;
		$_SESSION['nm_solicitante'] = $nm_solicitante;
		$_SESSION['nu_telefone_sol'] = $nu_telefone_sol;
		$_SESSION['dt_solicitacao'] = $dt_solicitacao;
		$_SESSION['nm_logra_sol'] = $nm_logra_sol;
		$_SESSION['nu_logra_sol'] = $nu_logra_sol;
		$_SESSION['nm_bairro_sol'] = $nm_bairro_sol;
		$_SESSION['nu_cep_sol'] = $nu_cep_sol;
		$_SESSION['nm_cidade_sol'] = $nm_cidade_sol;
		$_SESSION['nm_uf_sol'] = $nm_uf_sol;
		$_SESSION['te_obs_sol'] = $te_obs_sol;
		header ("location: ../solicitacao.php");
	
	} else if ( $nu_telefone_sol == "" ){
		$msg_excessao = "Telefone: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['id_servico'] = $id_servico;
		$_SESSION['nm_servico'] = $nm_servico;
		$_SESSION['nm_solicitante'] = $nm_solicitante;
		$_SESSION['nu_telefone_sol'] = $nu_telefone_sol;
		$_SESSION['dt_solicitacao'] = $dt_solicitacao;
		$_SESSION['nm_logra_sol'] = $nm_logra_sol;
		$_SESSION['nu_logra_sol'] = $nu_logra_sol;
		$_SESSION['nm_bairro_sol'] = $nm_bairro_sol;
		$_SESSION['nu_cep_sol'] = $nu_cep_sol;
		$_SESSION['nm_cidade_sol'] = $nm_cidade_sol;
		$_SESSION['nm_uf_sol'] = $nm_uf_sol;
		$_SESSION['te_obs_sol'] = $te_obs_sol;
		header ("location: ../solicitacao.php");
	
	} else if ( $dt_solicitacao == "" ){
		$msg_excessao = "Data: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['id_servico'] = $id_servico;
		$_SESSION['nm_servico'] = $nm_servico;
		$_SESSION['nm_solicitante'] = $nm_solicitante;
		$_SESSION['nu_telefone_sol'] = $nu_telefone_sol;
		$_SESSION['dt_solicitacao'] = $dt_solicitacao;
		$_SESSION['nm_logra_sol'] = $nm_logra_sol;
		$_SESSION['nu_logra_sol'] = $nu_logra_sol;
		$_SESSION['nm_bairro_sol'] = $nm_bairro_sol;
		$_SESSION['nu_cep_sol'] = $nu_cep_sol;
		$_SESSION['nm_cidade_sol'] = $nm_cidade_sol;
		$_SESSION['nm_uf_sol'] = $nm_uf_sol;
		$_SESSION['te_obs_sol'] = $te_obs_sol;
		header ("location: ../solicitacao.php");
		
	} else {
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_servico']);
		unset($_SESSION['nm_servico']);
		unset($_SESSION['nm_solicitante']);
		unset($_SESSION['nu_telefone_sol']);
		unset($_SESSION['dt_solicitacao']);
		unset($_SESSION['nm_logra_sol']);
		unset($_SESSION['nu_logra_sol']);
		unset($_SESSION['nm_bairro_sol']);
		unset($_SESSION['nu_cep_sol']);
		unset($_SESSION['nm_cidade_sol']);
		unset($_SESSION['nm_uf_sol']);
		unset($_SESSION['te_obs_sol']);
		
		$persistence->inserirSolicitacao($id_associado,$nm_associado,$id_servico,$nm_solicitante,$nu_telefone_sol,$dt_solicitacao,$nm_logra_sol,$nu_logra_sol,$nm_bairro_sol,$nu_cep_sol,$nm_cidade_sol,$nm_uf_sol,$te_obs_sol);
	
	}

}

if (isset($_POST['editarSolicitacao'])){
	
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_solicitacao']);
		unset($_SESSION['id_servico']);
		
		$id_associado = addslashes($_POST['id_associado']);
		$nm_associado = addslashes($_POST['nm_associado']);
		$id_servico = addslashes($_POST['id_servico']);
		$nm_servico = addslashes($_POST['nm_servico']);
		$nm_solicitante = trim(addslashes($_POST['nm_solicitante']));
		$nm_solicitante = strtoupper($nm_solicitante);
		$nu_telefone_sol = trim(addslashes($_POST['nu_telefone_sol']));
		$dt_solicitacao = addslashes($_POST['dt_solicitacao']);
		$nm_logra_sol = trim(addslashes($_POST['nm_logra_sol']));
		$nm_logra_sol = strtoupper($nm_logra_sol);
		$nu_logra_sol = trim(addslashes($_POST['nu_logra_sol']));
		$nm_bairro_sol = trim(addslashes($_POST['nm_bairro_sol']));
		$nm_bairro_sol = strtoupper($nm_bairro_sol);
		$nu_cep_sol = addslashes($_POST['nu_cep_sol']);			
		$nm_cidade_sol = trim(addslashes($_POST['nm_cidade_sol']));
		$nm_cidade_sol = strtoupper($nm_cidade_sol);
		$nm_uf_sol = addslashes($_POST['nm_uf_sol']);
		$te_obs_sol = addslashes($_POST['te_obs_sol']);
		$id_solicitacao = addslashes($_POST['id_solicitacao']);
		$opcao = addslashes($_POST['opcao']);


	$persistence->editarSolicitacao($id_solicitacao,$nm_associado,$opcao,$id_associado,$nm_associado,$id_servico,$nm_solicitante,$nu_telefone_sol,$dt_solicitacao,$nm_logra_sol,$nu_logra_sol,$nm_bairro_sol,$nu_cep_sol,$nm_cidade_sol,$nm_uf_sol,$te_obs_sol);

}

if (isset($_GET['abrirSolicitacaoEdit'])){
	
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_solicitacao']);
		unset($_SESSION['id_servico']);
		
		$id_solicitacao = addslashes($_GET['id_solicitacao']);
		$opcao = addslashes($_GET['opcao']);
		$nm_associado = addslashes($_GET['nm_associado']);
		$_SESSION['id_solicitacao'] = $id_solicitacao;
		$_SESSION['opcao'] = $opcao;
		$_SESSION['nm_associado'] = $nm_associado;
				
		header ("location: ../solicitacao_edit.php");

}	

if (isset($_GET['abrirSolicitacaoLista'])){
	
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		unset($_SESSION['nm_servico']);
		unset($_SESSION['nm_solicitante']);
		
		$_SESSION['botao_ree'] = 0;		
				
		header ("location: ../solicitacao_lista.php");

}	

if ( isset($_GET['imprimirSolicitacaoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
				
							
		header ("location: ../solicitacao_lista_pdf.php");
}
	
if ( isset($_GET['abrirServico']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_solicitacao']);
		unset($_SESSION['id_servico']);
		unset($_SESSION['id_curriculo']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nu_cpf']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['dt_nascimento']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['opcao']);
				
		unset($_SESSION['nm_logra']);
		unset($_SESSION['nu_logra']);				
		unset($_SESSION['te_email']);
		unset($_SESSION['nm_compl']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cep']);
		unset($_SESSION['nm_cidade']);
		unset($_SESSION['nm_uf']);
		unset($_SESSION['opcao']);
		unset($_SESSION['te_obs']);
		
		unset($_SESSION['nm_logra_sol']);
		unset($_SESSION['nu_logra_sol']);				
		unset($_SESSION['nm_bairro_sol']);
		unset($_SESSION['nu_cep_sol']);
		unset($_SESSION['nm_cidade_sol']);
		unset($_SESSION['nm_uf_sol']);
		unset($_SESSION['te_obs_sol']);
		
		
		
		header ("location: ../servico.php");
	}

if ( isset($_POST['inserirServico']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
				
		$nm_servico = trim(addslashes($_POST['nm_servico']));
		$nm_servico = strtoupper($nm_servico);
				
		if ( $nm_servico == ""){
		$msg_excessao = "Servi�o: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		
		header ("location: ../servico.php");
		
		} else {
		$persistence->inserirServico($nm_servico);
}
}

if ( isset($_GET['abrirServicoEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_servico']);
		unset($_SESSION['nm_servico']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
		
		$id_servico = addslashes($_GET['id_servico']);
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['id_servico'] = $id_servico;
		$_SESSION['opcao'] = $opcao;				
					
		header ("location: ../servico_edit.php");
}

if ( isset($_POST['editarServico']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
				
		$nm_servico = trim(addslashes($_POST['nm_servico']));
		$nm_servico = strtoupper($nm_servico);
		$id_servico = addslashes($_POST['id_servico']);
		$opcao = addslashes($_POST['opcao']);
				
		if ( $nm_servico == ""){
		$msg_excessao = "Servi�o: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		
		header ("location: ../servico.php");
		
		} else {
		$persistence->editarServico($id_servico,$opcao,$nm_servico);
}
}
	
	if ( isset($_GET['abrirServicoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
		unset($_SESSION['id_servico']);
		
					
		header ("location: ../servico_lista.php");
}

if ( isset($_GET['imprimirServicoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
				
							
		header ("location: ../servico_lista_pdf.php");
}

if ( isset($_GET['excluirServico'])){

		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$id_servico = addslashes($_GET['id_servico']);
		$opcao = addslashes($_GET['opcao']);
		
		if ( $persistence->validarExclucaoServico($id_servico) ){
		$msg_excessao = "Exclus�o n�o permitida";
		$_SESSION['msg_excessao'] = $msg_excessao;
		
		header ("location: ../servico_lista.php");
		
		} else {
		
		$persistence->excluirServico($id_servico,$opcao);
		}

}

if ( isset($_GET['excluirSolicitacao'])){

		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$id_solicitacao = addslashes($_GET['id_solicitacao']);
		$opcao = addslashes($_GET['opcao']);
				
		
		$persistence->excluirSolicitacao($id_solicitacao,$opcao);
		}



?>