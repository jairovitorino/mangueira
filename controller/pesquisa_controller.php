<?php
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();

if ( isset($_GET['abrirPesquisaData']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['id_tipo']);
		unset($_SESSION['opcao']);
		unset($_SESSION['nm_objeto']);
		unset($_SESSION['nu_termo']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['dt_nascimento']);
		unset($_SESSION['numero_termo']);
		
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['opcao'] = $opcao;
							
		header ("location: ../pesquisa_data.php");
}
if ( isset($_POST['pesquisarData']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['opcao']);
		
		$dt_inicio = addslashes($_POST['dt_inicio']);
		$dt_fim = addslashes($_POST['dt_fim']);
		$opcao = addslashes($_POST['opcao']);
		
		if ( ($dt_inicio == "") || ($dt_fim == "") ){
		$msg_excessao = "Data: Preenchimento obrigatório";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['opcao'] = $opcao;	
				
		header ("location: ../pesquisa_data.php");
		
		} else if ( $opcao == "lista" ){
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
				
		header ("location:  lava_lista.php");
		
		} else if ( $opcao == "matr" ){
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
				
		header ("location:  matricula_lista.php");
		
		} else if ( $opcao == "analitica" ){
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
				
		header ("location:  lava_lista_analitica.php");
		
		} else if ( $opcao == "reserva" ){
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
				
		header ("location:  reserva_lista.php");
				
		} else {
		$_SESSION['opcao'] = $opcao;					
		header ("location: ../pesquisa_data.php");
		}
}
if ( isset($_GET['abrirPesquisaObjeto']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_profissao_like']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		unset($_SESSION['nm_indicacao']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cpf']);
		unset($_SESSION['nm_logra']);
		unset($_SESSION['st_curriculo']);
		unset($_SESSION['nm_psico']);
		unset($_SESSION['nm_servico']);
		unset($_SESSION['nu_idade']);
				
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['opcao'] = $opcao;
							
		header ("location: ../pesquisa_objeto.php");
}

if ( isset($_POST['pesquisarObjeto']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['opcao']);
		unset($_SESSION['nm_logra']);
		//unset($_SESSION['nm_associado']);
										
		$nm_profissao = trim(addslashes($_POST['nm_profissao']));
		$nm_indicacao = trim(addslashes($_POST['nm_indicacao']));
		$nm_logra = trim(addslashes($_POST['nm_logra']));
		$nm_bairro = trim(addslashes($_POST['nm_bairro']));
		$nu_idade = trim(addslashes($_POST['nu_idade']));
		$nm_associado = trim(addslashes($_POST['nm_associado']));
		$nm_foliao = trim(addslashes($_POST['nm_foliao']));
		$nm_servico = trim(addslashes($_POST['nm_servico']));
		$opcao = addslashes($_POST['opcao']);
		
		if ( $opcao == "nome_curriculo"){
		$_SESSION['nm_associado_like'] = $nm_associado;						
		header ("location: ../curriculo_lista.php");		
		
		} else if ( $opcao == "profissao_curriculo"){
		$_SESSION['nm_profissao_like'] = $nm_profissao;				
		header ("location: ../curriculo_lista.php");
		
		} else if ( $opcao == "indicacao_curriculo"){
		$_SESSION['nm_indicacao'] = $nm_indicacao;				
		header ("location: ../curriculo_lista.php");
		
		} else if ( $opcao == "logra_curriculo"){
		$_SESSION['nm_logra'] = $nm_logra;				
		header ("location: ../curriculo_lista.php");
		
		} else if ( $opcao == "bairro_curriculo"){
		$_SESSION['nm_bairro'] = $nm_bairro;				
		header ("location: ../curriculo_lista.php");
		
		} else if ( $opcao == "idade_curriculo"){
		$_SESSION['nu_idade'] = $nu_idade;				
		header ("location: ../curriculo_lista.php");
		
		//--------------------------------------------------------------------
		
		} else if ( $opcao == "nome_associado"){
		$_SESSION['nm_associado'] = $nm_associado;						
		header ("location: ../associado_lista.php");
		
		} else if ( $opcao == "bairro_associado"){
		$_SESSION['nm_bairro'] = $nm_bairro;						
		header ("location: ../associado_lista.php");
		
		} else if ( $opcao == "logra_associado"){
		$_SESSION['nm_logra'] = $nm_logra;						
		header ("location: ../associado_lista.php");
		
		//----------------------------------------------------------------
		
		} else if ( $opcao == "nome_psico"){
		$_SESSION['nm_associado_like'] = $nm_associado;						
		header ("location: ../psico_lista.php");
		
		//--------------------------------------------------
		
		} else if ( $opcao == "nome_treino"){
		$_SESSION['nm_associado_like'] = $nm_associado;						
		header ("location: ../treino_lista.php");
		
		//-------------------------------------------------
		
		} else if ( $opcao == "foliao_bloco"){
		$_SESSION['nm_foliao'] = $nm_foliao;						
		header ("location: ../bloco_lista.php");
		
		} else if ( $opcao == "responsavel_bloco"){
		$_SESSION['nm_associado_like'] = $nm_associado;						
		header ("location: ../bloco_lista.php");
		
		//---------------------------------------------------
		
		} else if ( $opcao == "nome_solicitacao"){
		$_SESSION['nm_associado_like'] = $nm_associado;						
		header ("location: ../solicitacao_lista.php");
		
		} else if ( $opcao == "nome_servico"){
		$_SESSION['nm_servico'] = $nm_servico;						
		header ("location: ../solicitacao_lista.php");
						
		} else {
		
		header ("location: ../pesquisa_objeto.php");
		}
	}
?>