<?php
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();

if ( isset($_GET['cancelarOperacao']) ) {		
	
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nu_cpf']);
		unset($_SESSION['id_curriculo']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_profissao_like']);
		unset($_SESSION['checkbox']);
		unset($_SESSION['id_sexo']);
		unset($_SESSION['nm_sexo']);
		unset($_SESSION['id_associado']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		unset($_SESSION['id_psicanalista']);				
		unset($_SESSION['nm_psicanalista']);
		unset($_SESSION['id_psico']);				
		unset($_SESSION['dia']);
		unset($_SESSION['mes']);
		unset($_SESSION['ano']);
		unset($_SESSION['dt_solicitacao']);
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['hr_psico']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['nu_whatsapp']);
		unset($_SESSION['nu_rg']);
		unset($_SESSION['nome_psico']);
		unset($_SESSION['te_conselho']);
		unset($_SESSION['id_treino']);
		unset($_SESSION['st_curriculo']);
		unset($_SESSION['nm_foliao']);
		unset($_SESSION['nu_rg_foliao']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['id_solicitacao']);
		unset($_SESSION['id_servico']);
		unset($_SESSION['nm_servico']);
		unset($_SESSION['nm_solicitante']);
		unset($_SESSION['nm_indicacao']);
		unset($_SESSION['nu_idade']);
		unset($_SESSION['id_categoria']);
		unset($_SESSION['nm_categoria']);
		unset($_SESSION['id_acao']);
		unset($_SESSION['st_responsavel']);
		unset($_SESSION['botao_ree']);
				
		unset($_SESSION['nm_logra_sol']);
		unset($_SESSION['nu_logra_sol']);				
		unset($_SESSION['nm_bairro_sol']);
		unset($_SESSION['nu_cep_sol']);
		unset($_SESSION['nm_cidade_sol']);
		unset($_SESSION['nm_uf_sol']);
		unset($_SESSION['te_obs_sol']);
											
		unset($_SESSION['nm_logra']);
		unset($_SESSION['nu_logra']);				
		unset($_SESSION['te_email']);
		unset($_SESSION['nm_compl']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cep']);
		unset($_SESSION['nm_cidade']);
		unset($_SESSION['nm_uf']);
		unset($_SESSION['opcao']);
		unset($_SESSION['te_obs']);		
				
		header ("location: ../index.php");
	}


?>