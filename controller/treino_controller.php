<?php
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();

if ( isset($_GET['abrirCurso']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_psico']);
		unset($_SESSION['nm_psico']);
		unset($_SESSION['id_curso']);
		unset($_SESSION['nm_curso']);		
		unset($_SESSION['hr_psico']);
		unset($_SESSION['id_associado']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['id_psicanalista']);				
		unset($_SESSION['nm_psicanalista']);				
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['id_tipo']);
		unset($_SESSION['opcao']);
		unset($_SESSION['nu_rg']);
		unset($_SESSION['nu_cpf']);		
		
		header ("location: ../curso.php");
	}

if ( isset($_GET['abrirTreino']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_psico']);
		unset($_SESSION['id_treino']);
		unset($_SESSION['nm_psico']);
		unset($_SESSION['hr_psico']);
		unset($_SESSION['id_associado']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['id_psicanalista']);				
		unset($_SESSION['nm_psicanalista']);				
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['id_tipo']);
		unset($_SESSION['opcao']);
		unset($_SESSION['nu_rg']);
		unset($_SESSION['nu_cpf']);		
		
		$id_associado = addslashes($_GET['id_associado']);
							
		//header ("location: ../psico.php");
		$persistence->abrirTreino($id_associado);
}


if ( isset($_POST['inserirCurso']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_curso']);
		unset($_SESSION['nm_curso']);
		unset($_SESSION['nu_carga']);
		
		$nm_curso = trim(addslashes($_POST['nm_curso']));
		$nm_curso = strtoupper($nm_curso);
				
		if ( $nm_curso == "" ){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		header ("location: ../curso.php");
						
		} else {	
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_curso']);
				
		$persistence->inserirCurso($nm_curso);
	}
}

if ( isset($_POST['editarCurso']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_curso']);
		unset($_SESSION['nm_curso']);
		unset($_SESSION['nu_carga']);
		
		$nm_curso = trim(addslashes($_POST['nm_curso']));
		$nm_curso = strtoupper($nm_curso);
		$id_curso = addslashes($_POST['id_curso']);
		$opcao = addslashes($_POST['opcao']);
				
		if ( $nm_curso == "" ){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		header ("location: ../curso.php");
						
		} else {	
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_curso']);
				
		$persistence->editarCurso($id_curso,$opcao,$nm_curso);
	}
}

if ( isset($_GET['abrirCursoEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
		$id_curso = addslashes($_GET['id_curso']);
		$_SESSION['id_curso'] = $id_curso;
							
		header ("location: ../curso_edit.php");
}

if ( isset($_GET['excluirCurso']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
						
		$id_curso = addslashes($_GET['id_curso']);
		$opcao = addslashes($_GET['opcao']);
		
		if ( $persistence->validarExclucaoCurso($id_curso) ){
		$msg_excessao = "Exclus�o n�o permitida";
		$_SESSION['msg_excessao'] = $msg_excessao;
		
		header ("location: ../curso_lista.php");
		
		} else {
									
		$persistence->excluirCurso($id_curso,$opcao);
}
}

if ( isset($_POST['inserirTreino']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		
		$id_associado = addslashes($_POST['id_associado']);
		$nm_associado = addslashes($_POST['nm_associado']);
		$dt_inicio = addslashes($_POST['dt_inicio']);
		$dt_fim = addslashes($_POST['dt_fim']);
		$hr_treino = addslashes($_POST['hr_treino']);
		$id_curso = addslashes($_POST['id_curso']);
		$nm_curso = addslashes($_POST['nm_curso']);
		$nu_carga = addslashes($_POST['nu_carga']);
		$te_obs = addslashes($_POST['te_obs']);
				
		if ( $dt_inicio == "" ){
		$msg_excessao = "Data In�cio: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino.php");
		
		} else if ( $dt_fim == "" ){
		$msg_excessao = "Data Fim: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino.php");
		
		} else if ( $hr_treino == "" ){
		$msg_excessao = "Hora: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino.php");
		
		} else if ( $id_curso == 0 ){
		$msg_excessao = "Curso: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino.php");
		
		} else if ( $nu_carga == 0 ){
		$msg_excessao = "Carga Hor�ria: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino.php");
		
		} else if ( $persistence->validarTreino($dt_inicio,$dt_fim,$id_associado) ){
		$msg_excessao = "Associado em treinamento";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino.php");
										
		} else {	
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['hr_treino']);
		unset($_SESSION['id_curso']);
		unset($_SESSION['nm_curso']);
		unset($_SESSION['nu_carga']);
		unset($_SESSION['te_obs']);
				
		$persistence->inserirTreino($id_associado,$nm_associado,$dt_inicio,$dt_fim,$hr_treino,$id_curso,$nu_carga,$te_obs);
	}
}

if ( isset($_POST['editarTreino']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		
		$id_associado = addslashes($_POST['id_associado']);
		$nm_associado = addslashes($_POST['nm_associado']);
		$dt_inicio = addslashes($_POST['dt_inicio']);
		$dt_fim = addslashes($_POST['dt_fim']);
		$hr_treino = addslashes($_POST['hr_treino']);
		$id_curso = addslashes($_POST['id_curso']);
		$nm_curso = addslashes($_POST['nm_curso']);
		$nu_carga = addslashes($_POST['nu_carga']);
		$te_obs = addslashes($_POST['te_obs']);
		$opcao = addslashes($_POST['opcao']);
		$id_treino = addslashes($_POST['id_treino']);
				
		if ( $dt_inicio == "" ){
		$msg_excessao = "Data In�cio: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino_edit.php");
		
		} else if ( $dt_fim == "" ){
		$msg_excessao = "Data Fim: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino_edit.php");
		
		} else if ( $hr_treino == "" ){
		$msg_excessao = "Hora: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino_edit.php");
		
		} else if ( strlen( $hr_treino ) < 5 ){
		$msg_excessao = "Hora inv�lida";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino_edit.php");
		
		} else if ( (substr($hr_treino,0,2) > 23) || (substr($hr_treino,3,2) > 59) ){ // 12:12
		$msg_excessao = "Hora: formato inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino_edit.php");
		
		} else if ( $id_curso == 0 ){
		$msg_excessao = "Curso: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino_edit.php");
		
		} else if ( $nu_carga == 0 ){
		$msg_excessao = "Carga Hor�ria: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_treino'] = $hr_treino;
		$_SESSION['id_curso'] = $id_curso;
		$_SESSION['nm_curso'] = $nm_curso;
		$_SESSION['nu_carga'] = $nu_carga;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../treino_edit.php");		
										
		} else {	
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['hr_treino']);
		unset($_SESSION['id_curso']);
		unset($_SESSION['nm_curso']);
		unset($_SESSION['nu_carga']);
		unset($_SESSION['te_obs']);
				
		$persistence->editarTreino($opcao,$id_treino,$id_associado,$nm_associado,$dt_inicio,$dt_fim,$hr_treino,$id_curso,$nu_carga,$te_obs);
	}
}


if ( isset($_GET['abrirTreinoEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
		$id_treino = addslashes($_GET['id_treino']);
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['id_treino'] = $id_treino;
		$_SESSION['opcao'] = $opcao;
							
		header ("location: ../treino_edit.php");
}

if ( isset($_GET['abrirTreinoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
		
		$_SESSION['botao_ree'] = 0;		
							
		header ("location: ../treino_lista.php");
}

if ( isset($_GET['imprimirTreinoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
									
		header ("location: ../treino_lista_pdf.php");
}

if ( isset($_GET['excluirTreino']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);	
		
		$id_treino = addslashes($_GET['id_treino']);		
		$opcao = addslashes($_GET['opcao']);
		
		$persistence->excluirTreino($id_treino,$opcao);
}

if ( isset($_GET['abrirCursoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
							
		header ("location: ../curso_lista.php");
}

if ( isset($_GET['imprimirCursoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
							
		header ("location: ../curso_lista_pdf.php");
}


if ( isset($_GET['']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		
							
		header ("location: ../curso_lista_pdf.php");
}

?>