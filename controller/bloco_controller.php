<?php 
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();

	
if ( isset($_GET['abrirBloco']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_bloco']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nu_cpf']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['dt_nascimento']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_foliao']);
				
		unset($_SESSION['nm_logra']);
		unset($_SESSION['nu_logra']);				
		unset($_SESSION['te_email']);
		unset($_SESSION['nm_compl']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cep']);
		unset($_SESSION['nm_cidade']);
		unset($_SESSION['nm_uf']);
		unset($_SESSION['opcao']);
		unset($_SESSION['te_obs']);
		
		$id_associado = addslashes($_GET['id_associado']);		
		
		$persistence->abrirBloco($id_associado);
		//header ("location: ../curriculo.php");
	}
	
	if ( isset($_POST['inserirBloco']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
				
		$id_associado = addslashes($_POST['id_associado']);
		$nm_associado = addslashes($_POST['nm_associado']);
		$st_responsavel = addslashes($_POST['st_responsavel']);
		$ck_trabalho = addslashes($_POST['ck_trabalho']);
		$ck_trabalho == "" ? $ck_trabalho = 0 : $ck_trabalho = 1;
		$nm_foliao = trim(addslashes($_POST['nm_foliao']));
		$nm_foliao = strtoupper($nm_foliao);
		$dia = addslashes($_POST['dia']);
		$mes = addslashes($_POST['mes']);
		$id_ano = addslashes($_POST['id_ano']);
		$nu_ano = addslashes($_POST['nu_ano']);
		$dt_nascimento = $dia."/".$mes."/".$id_ano ;
		$nu_ano_bloco = addslashes($_POST['nu_ano_bloco']);
		$id_sexo = addslashes($_POST['id_sexo']);
		$nm_sexo = addslashes($_POST['nm_sexo']);
		$nu_rg_foliao = addslashes($_POST['nu_rg_foliao']);
		$st_parentesco = addslashes($_POST['st_parentesco']);
		$nu_ano = addslashes($_POST['nu_ano']);
		$te_obs = addslashes($_POST['te_obs']);
		
		//$dt_nascimento = substr($dt_nascimento,6,4)."/".substr($dt_nascimento,3,2)."/".substr($dt_nascimento,0,2);
		
		// separando dd, mm, yyyy
    	list($dia, $mes, $ano) = explode('/', $dt_nascimento);
		 // data atual
    	$hoje = mktime(0, 0, 0, @date('m'), @date('d'), @date('Y'));
		 // Descobre a unix timestamp da data de nascimento do fulano
    	$nascimento = mktime( 0, 0, 0, $dia, $mes, $ano);
		 // c�lculo
    	$idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
				
		if ( $st_responsavel == "" ){
		$msg_excessao = "Rela��o: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;		
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( $nm_foliao == "" ){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;		
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");		
		
		} else if ( ($dia == "") || ($mes == "") || ($id_ano == "") ){
		$msg_excessao = "Nascimento: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( ($id_ano%4!=0) && ($dia == "29")  ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( ($mes == "02") && ($dia == 30) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( ($mes == "02") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( ($mes == "04") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( ($mes == "06") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( ($mes == "09") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( ($mes == "11") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( $id_sexo == "" ){
		$msg_excessao = "Sexo: Preenchimento obrigatorio ";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( ($idade < 1) || ($idade > 8) ){
		$msg_excessao = "Idade n�o permitida ";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
								
		} else if ( $nu_rg_foliao == "" ){
		$msg_excessao = "RG: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( $nu_ano_bloco == 0000 ){
		$msg_excessao = "Ano: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( strlen($nu_ano_bloco) < 4 ){
		$msg_excessao = "Ano: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( $st_parentesco == "" ){
		$msg_excessao = "Parentesco: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else if ( $persistence->validarBloco($nu_rg_foliao,$nu_ano) ){
		$msg_excessao = "RG j� cadastrado neste ano";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['st_responsavel'] = $st_responsavel;
		$_SESSION['ck_trabalho'] = $ck_trabalho;
		$_SESSION['nm_foliao'] = $nm_foliao;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_ano_bloco'] = $nu_ano_bloco;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['nu_rg_foliao'] = $nu_rg_foliao;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['st_parentesco'] = $st_parentesco;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../bloco.php");
		
		} else {		
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['st_responsavel']);
		unset($_SESSION['ck_trabalho']);
		unset($_SESSION['nm_foliao']);
		unset($_SESSION['dia']);
		unset($_SESSION['mes']);
		unset($_SESSION['id_ano']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['nu_ano_bloco']);
		unset($_SESSION['id_sexo']);
		unset($_SESSION['nm_sexo']);
		unset($_SESSION['nu_rg_foliao']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['st_parentesco']);
		unset($_SESSION['te_obs']);
		
		//$dt_nascimento = substr($dt_nascimento,8,2)."/".substr($dt_nascimento,5,2)."/".substr($dt_nascimento,0,4);
		
		$persistence->inserirBloco($id_associado,$nm_associado,$st_responsavel,$ck_trabalho,$nm_foliao,$dt_nascimento,$id_sexo,$nu_rg_foliao,$nu_ano_bloco,$st_parentesco,$te_obs);
		}
	}
	
if ( isset($_GET['abrirBlocoEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);	
		
		$id_bloco = addslashes($_GET['id_bloco']);
		$nm_foliao = addslashes($_GET['nm_foliao']);
		$opcao = addslashes($_GET['opcao']);
				
		$_SESSION['id_bloco'] = $id_bloco;
		$_SESSION['opcao'] = $opcao;
		$_SESSION['nm_foliao'] = $nm_foliao;
				
		header ("location: ../bloco_edit.php");
}

if ( isset($_POST['editarBloco']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);	
		
		$id_bloco = addslashes($_POST['id_bloco']);
		$id_associado = addslashes($_POST['id_associado']);
		$st_responsavel = addslashes($_POST['st_responsavel']);
		$ck_trabalho = addslashes($_POST['ck_trabalho']);
		$ck_trabalho == "" ? $ck_trabalho = "0" : $ck_trabalho = "1";
		$te_obs = addslashes($_POST['te_obs']);
		$opcao = addslashes($_POST['opcao']);
				
		$persistence->editarBloco($id_bloco,$id_associado,$st_responsavel,$ck_trabalho,$te_obs,$opcao);
}

if ( isset($_GET['excluirBloco']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
		unset($_SESSION['st_curriculo']);
		unset($_SESSION['id_bloco']);
		
		$id_bloco = addslashes($_GET['id_bloco']);
		$j = addslashes($_GET['j']);
		$opcao = addslashes($_GET['opcao']);
							
		$persistence->excluirBloco($id_bloco,$opcao,$j);
}

if ( isset($_GET['ticarBloco']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
		unset($_SESSION['st_curriculo']);
		unset($_SESSION['id_bloco']);
		
		$id_bloco = addslashes($_GET['id_bloco']);
		$nu_rg_foliao = addslashes($_GET['nu_rg_foliao']);
		$nm_associado = addslashes($_GET['nm_associado']);
		
		$nu_ano = date("Y");
		
		if ( $persistence->validarTicarBloco($nu_rg_foliao,$nu_ano)){
		$msg_excessao = "Inscri��o j� renovada";
		$_SESSION['msg_excessao'] = $msg_excessao;		
		header ("location: ../bloco_lista.php");
		
		} else {
									
		$persistence->ticarBloco($id_bloco,$nm_associado);
}
}
if ( isset($_GET['abrirBlocoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_foliao']);
		unset($_SESSION['st_curriculo']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		
		$_SESSION['botao_ree'] = 0;		
					
		header ("location: ../bloco_lista.php");
}

if ( isset($_GET['imprimirBlocoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
				
							
		header ("location: ../bloco_lista_pdf.php");
}

if ( isset($_GET['imprimirTermo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$id_bloco = addslashes($_GET['id_bloco']);
		$_SESSION['id_bloco'] = $id_bloco;
									
		header ("location: ../termo_pdf.php");
}

if ( isset($_GET['excluirBloco']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);	
		
		$id_bloco = addslashes($_GET['id_bloco']);		
		$opcao = addslashes($_GET['opcao']);
		
		$persistence->excluirBloco($id_bloco,$opcao);
}

?>