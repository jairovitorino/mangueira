<?php
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();

if ( isset($_GET['abrirAssociadoLookup']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_associado']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['id_sexo']);
		unset($_SESSION['nm_sexo']);
		unset($_SESSION['dt_nascimento']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['nu_whatsapp']);
		unset($_SESSION['id_sexo']);
		unset($_SESSION['nm_sexo']);
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['id_tipo']);
		unset($_SESSION['opcao']);
		unset($_SESSION['nu_rg']);
		unset($_SESSION['nu_cpf']);		
		
		unset($_SESSION['nm_logra']);
		unset($_SESSION['nu_logra']);				
		unset($_SESSION['te_email']);
		unset($_SESSION['nm_compl']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cep']);
		unset($_SESSION['nm_cidade']);
		unset($_SESSION['nm_uf']);
		unset($_SESSION['opcao']);
		unset($_SESSION['te_obs']);			
		
							
		header ("location: ../associado_lookup.php");
}

if ( isset($_POST['lookupAssociado']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_associado']);
		unset($_SESSION['nu_cep']);			
		
		$nu_cpf = trim(addslashes($_POST['nu_cpf']));
		$nu_cep = trim(addslashes($_POST['nu_cep']));
				
		if ( $nu_cpf == ""){
		$msg_excessao = "CPF: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cep'] = $nu_cep;
		header ("location: ../associado_lookup.php");
				
		} else if (($nu_cpf != "") && (!is_numeric($nu_cpf))){
		$msg_excessao = "CPF: caracteres n�o aceito";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		$_SESSION['nu_cep'] = $nu_cep;
		header ("location: ../associado_lookup.php");
					
		} else if ( $persistence->lookupAssociado($nu_cpf) ){
		$msg_excessao = "CPF j� cadastrado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		$_SESSION['nu_cep'] = $nu_cep;		
		header ("location: ../associado_lookup.php");
		
		} else if ( !$persistence->validCPF($nu_cpf) ){
		$msg_excessao = "CPF: d�gito inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		$_SESSION['nu_cep'] = $nu_cep;		
		header ("location: ../associado_lookup.php");
		
		} else if ( strlen($nu_cpf) < 11){
		$msg_excessao = "CPF: tamanho inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		$_SESSION['nu_cep'] = $nu_cep;
		header ("location: ../associado_lookup.php");
		
		} else if ( $nu_cep == ""){
		$msg_excessao = "CEP: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		$_SESSION['nu_cep'] = $nu_cep;
		header ("location: ../associado_lookup.php");
		
		} else if ( strlen($nu_cep) < 8){
		$msg_excessao = "CEP: tamanho inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		$_SESSION['nu_cep'] = $nu_cep;
		header ("location: ../associado_lookup.php");
		
				
		} else if (($nu_cep != "") && (!is_numeric($nu_cep))){
		$msg_excessao = "CEP: caracteres n�o aceito";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		$_SESSION['nu_cep'] = $nu_cep;
		header ("location: ../associado_lookup.php");				
		
		} else {
		
		//-------------------------------------------------------------------------
		/*function buscaCep($cep){
		global $retorno;
		$resultado file_get_contents("http://republicavirtual.com.br/web_cep.php?cep=$cep&formato=query_string");
		
		//echo $logradouro = urldecode($resultado);
		//echo "<br />";
		$retorno = parse_str($resultado,$retorno);
		//print_r($retorno);
		return $retorno;
		// hablitar a linha php.ini --> php_curl.dll
		}
		buscaCep($nu_cep);*/
		//------------------------------------------------------------------------
			
		$_SESSION['nu_cpf'] = $nu_cpf;
		//$_COOKIE['nm_logra'] = $retorno['logradouro'];
		$_SESSION['nu_cep'] = $nu_cep;
								
		header ("location: ../associado.php");
	}
	}

if ( isset($_POST['inserirAssociado']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$nm_associado = trim(addslashes($_POST['nm_associado']));
		$nm_associado = strtoupper($nm_associado);
		$id_sexo = addslashes($_POST['id_sexo']);
		$nm_sexo = addslashes($_POST['nm_sexo']);
		$dia = addslashes($_POST['dia']);
		$mes = addslashes($_POST['mes']);
		$id_ano = addslashes($_POST['id_ano']);
		$nu_ano = addslashes($_POST['nu_ano']);
		$dt_nascimento = $dia."/".$mes."/".$id_ano ;
		$nu_telefone = trim(addslashes($_POST['nu_telefone']));
		$nu_whatsapp = addslashes($_POST['nu_whatsapp']);		
		$nu_cpf = addslashes($_POST['nu_cpf']);
		$nm_logra = trim(addslashes($_POST['nm_logra']));
		$nm_logra = strtoupper($nm_logra);
		$nu_logra = addslashes($_POST['nu_logra']);
		$nm_compl = trim(addslashes($_POST['nm_compl']));
		$nm_compl = strtoupper($nm_compl);		
		$nm_bairro = trim(addslashes($_POST['nm_bairro']));
		$nm_bairro = strtoupper($nm_bairro);
		$nu_cep = addslashes($_POST['nu_cep']);
		$nm_cidade = trim(addslashes($_POST['nm_cidade']));
		$nm_cidade = strtoupper($nm_cidade);
		$nm_uf = addslashes($_POST['nm_uf']);
		$te_obs = addslashes($_POST['te_obs']);
		$upload  = $_FILES['upload'];
		$te_imagem = addslashes($_POST['te_imagem']);
								
		$upextensao['extensoes'] = array('jpg');
		$extensao = strtolower(end(explode('.', $_FILES['upload']['name'])));
		$uptamanho['tamanho'] = 2097152; // 2Mb
		
		$imagem = $_FILES["upload"]["name"]; //pega o nome do arquivo
   		$temp_imagem = $_FILES["upload"]["tmp_name"]; //pega o "temp" do arquivo
   		$tipo = $_FILES["upload"]["type"]; //pega o tipo do arquivo
   		$tamanho = $_FILES["upload"]["size"]; //pega o tamanho do arquivo
   		$t_maximo = 2000000; //tamanho m�ximo do arquivo - em bytes
   		$uploaddir = '../fotos/';
		$uploadfile = $uploaddir . $_FILES['upload']['name'];				
		
		$imagem = isset($_FILES['upload']) ? $_FILES['upload'] : FALSE;
		
		if ( $nm_associado == "" ){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");	
		
		} else if ( $id_sexo == 0 ){
		$msg_excessao = "Sexo: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($dia == "") || ($mes == "") || ($id_ano == "") ){
		$msg_excessao = "Nascimento: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($id_ano%4!=0) && ($dia == "29")  ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($mes == "02") && ($dia == 30) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($mes == "02") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($mes == "04") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($mes == "06") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($mes == "09") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($mes == "11") && ($dia == 31) ){
		$msg_excessao = "Nascimento: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($nu_whatsapp != "") && (strlen( $nu_whatsapp ) < 11) ){
		$msg_excessao = "Whatsapp: tamanho inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($nu_whatsapp != "") && (!is_numeric( $nu_whatsapp) ) ){
		$msg_excessao = "Whatsapp: caractere inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");							
				
		} else if ( $nm_logra == "" ){
		$msg_excessao = "Logradouro: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");			
				
		} else if ( $nm_bairro == "" ){
		$msg_excessao = "Bairro: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ($imagem == ""){
		$msg_excessao = "Escolha um arquivo � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;		
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ereg("[][><}{)(:;,!?*%&#@]", $imagem) ){
		$msg_excessao = "O nome do arquivo cont�m caracteres inv�lidos";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
						
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "Foto: tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( ($te_imagem != "") && (!eregi("[gif|jpeg|jpg]", $tipo)) ){
		$msg_excessao = "Foto: Tipo de arquivo inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( is_dir($imagem) ){
		$msg_excessao = "Foto: Selecione um <u>arquivo</u> � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");
		
		} else if ( file_exists("$uploaddir"."$imagem") ){
		$msg_excessao = "Foto: J� existe um arquivo com este nome $imagem, por favor, renomeie-o";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dia'] = $dia;
		$_SESSION['mes'] = $mes;
		$_SESSION['id_ano'] = $id_ano;
		$_SESSION['nu_ano'] = $nu_ano;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado.php");	
		
		} else {
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['id_sexo']);
		unset($_SESSION['nm_sexo']);
		unset($_SESSION['dia']);
		unset($_SESSION['mes']);
		unset($_SESSION['id_ano']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['nu_whatsapp']);
		unset($_SESSION['nu_cpf']);
		unset($_SESSION['nm_logra']);
		unset($_SESSION['nu_logra']);
		unset($_SESSION['nm_compl']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cep']);
		unset($_SESSION['nm_cidade']);
		unset($_SESSION['nm_uf']);
		unset($_SESSION['te_obs']);
		
		move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile) ;
		
		$persistence->inserirAssociado($nm_associado,$id_sexo,$dt_nascimento,$nu_telefone,$nu_whatsapp,$nu_cpf,$nm_logra,$nu_logra,$nm_compl,$nm_bairro,$nu_cep,$nm_cidade,$nm_uf,$te_obs,$te_imagem);
		}
}

if ( isset($_GET['abrirAssociadoEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
				
		$id_associado = addslashes($_GET['id_associado']);
		$_SESSION['id_associado'] = $id_associado;
		
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['opcao'] = $opcao;
							
		header ("location: ../associado_edit.php");
}

if ( isset($_POST['editarAssociado']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$nm_associado = trim(addslashes($_POST['nm_associado']));
		$nm_associado = strtoupper($nm_associado);
		$id_sexo = addslashes($_POST['id_sexo']);
		$nm_sexo = addslashes($_POST['nm_sexo']);
		$dt_nascimento = addslashes($_POST['dt_nascimento']);
		$nu_telefone = trim(addslashes($_POST['nu_telefone']));
		$nu_whatsapp = addslashes($_POST['nu_whatsapp']);			
		$nu_rg = addslashes($_POST['nu_rg']);
		$nm_logra = trim(addslashes($_POST['nm_logra']));
		$nm_logra = strtoupper($nm_logra);
		$nu_logra = addslashes($_POST['nu_logra']);
		$nm_compl = trim(addslashes($_POST['nm_compl']));
		$nm_compl = strtoupper($nm_compl);		
		$nm_bairro = trim(addslashes($_POST['nm_bairro']));
		$nm_bairro = strtoupper($nm_bairro);
		$nu_cep = addslashes($_POST['nu_cep']);
		$nm_cidade = trim(addslashes($_POST['nm_cidade']));
		$nm_cidade = strtoupper($nm_cidade);
		$nm_uf = addslashes($_POST['nm_uf']);
		$te_obs = addslashes($_POST['te_obs']);
		$upload  = $_FILES['upload'];
		$te_imagem = addslashes($_POST['te_imagem']);
		$te_imagem_arq = addslashes($_POST['te_imagem_arq']);
		$id_associado = addslashes($_POST['id_associado']);
		$opcao = addslashes($_POST['opcao']);
		$checkbox = addslashes($_POST['checkbox']);
		$checkbox2 = addslashes($_POST['checkbox2']);
						
		$upextensao['extensoes'] = array('pdf','jpg');
		$extensao = strtolower(end(explode('.', $_FILES['upload']['name'])));
		$uptamanho['tamanho'] = 2097152; // 2Mb
		
		$imagem = $_FILES["upload"]["name"]; //pega o nome do arquivo
   		$temp_imagem = $_FILES["upload"]["tmp_name"]; //pega o "temp" do arquivo
   		$tipo = $_FILES["upload"]["type"]; //pega o tipo do arquivo
   		$tamanho = $_FILES["upload"]["size"]; //pega o tamanho do arquivo
   		$t_maximo = 2000000; //tamanho m�ximo do arquivo - em bytes
   		$uploaddir = '../fotos/';
		$uploadfile = $uploaddir . $_FILES['upload']['name'];				
		
		$imagem = isset($_FILES['upload']) ? $_FILES['upload'] : FALSE;
		
		if ($imagem == ""){
		$msg_excessao = "Escolha um arquivo � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else if ( ereg("[][><}{)(:;,!?*%&#@]", $imagem) ){
		$msg_excessao = "O nome do arquivo cont�m caracteres inv�lidos";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else if ( $nm_associado == "" ){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");	
		
		} else if ( $id_sexo == 0 ){
		$msg_excessao = "Sexo: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else if ( ($checkbox2) && ($persistence->lookupAssociado($nu_rg)) ){
		$msg_excessao = "RG j� cadastrado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");			
				
		} else if ( ($nu_whatsapp != "") && (strlen( $nu_whatsapp ) < 11) ){
		$msg_excessao = "Whatsapp: tamanho inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else if ( ($nu_whatsapp != "") && (!is_numeric( $nu_whatsapp )) ){
		$msg_excessao = "Whatsapp: caractere inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");							
				
		} else if ( $nm_logra == "" ){
		$msg_excessao = "Logradouro: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");		
				
		} else if ( $nm_bairro == "" ){
		$msg_excessao = "Bairro: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");			
				
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "Foto: tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "Foto: tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else if ( ($te_imagem != "") && (!eregi("[gif|jpeg|jpg]", $tipo)) ){
		$msg_excessao = "Foto: Tipo de arquivo inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else if ( is_dir($imagem) ){
		$msg_excessao = "Foto: Selecione um <u>arquivo</u> � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else if ( file_exists("$uploaddir"."$imagem") ){
		$msg_excessao = "Foto: J� existe um arquivo com este nome $imagem, por favor, renomeie-o";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");	
		
		} else if ( ($dt_nascimento == "00/00/0000") || ($dt_nascimento == "") ){
		$msg_excessao = "Nascimento: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['dt_nascimento'] = $dt_nascimento;
		$_SESSION['nu_telefone'] = $nu_telefone;
		$_SESSION['nu_whatsapp'] = $nu_whatsapp;
		$_SESSION['nu_rg'] = $nu_rg;
		$_SESSION['nm_logra'] = $nm_logra;
		$_SESSION['nu_logra'] = $nu_logra;
		$_SESSION['nm_compl'] = $nm_compl;
		$_SESSION['nm_bairro'] = $nm_bairro;
		$_SESSION['nu_cep'] = $nu_cep;
		$_SESSION['nm_cidade'] = $nm_cidade;
		$_SESSION['nm_uf'] = $nm_uf;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../associado_edit.php");
		
		} else {
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['id_sexo']);
		unset($_SESSION['nm_sexo']);
		unset($_SESSION['dt_nascimento']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['nu_whatsapp']);
		unset($_SESSION['nu_rg']);
		unset($_SESSION['nm_logra']);
		unset($_SESSION['nu_logra']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cep']);
		unset($_SESSION['nm_cidade']);
		unset($_SESSION['nm_uf']);
		unset($_SESSION['te_obs']);
		
		move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile) ;
		
		$persistence->editarAssociado($checkbox,$id_associado,$opcao,$nm_associado,$id_sexo,$dt_nascimento,$nu_telefone,$nu_whatsapp,$nu_rg,$nm_logra,$nu_logra,$nm_compl,$nm_bairro,$nu_cep,$nm_cidade,$nm_uf,$te_obs,$te_imagem,$te_imagem_arq);
		}
}

if ( isset($_GET['abrirSms']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
				
		$id_associado = addslashes($_GET['id_associado']);
		
		if ( $persistence->smsExiste($id_associado) ){
		$msg_excessao = "Associado n�o possui Whatsapp cadastrado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		
		header ("location: ../associado_lista.php");			
		} else {
		
		$persistence->abrirSms($id_associado);
}
}

if ( isset($_GET['excluirAssociado']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_associado']);
		unset($_SESSION['nm_associado']);				
		
		$id_associado = addslashes($_GET['id_associado']);				
		$opcao = addslashes($_GET['opcao']);
		$te_imagem = addslashes($_GET['te_imagem']);
		//$te_imagem = substr($te_imagem,12);
		
		$arquivo = $te_imagem;		
		/* Diretorio que deve ser lido */
    	$dir = '../fotos/';   	 
		$documemnto = $dir.$arquivo; 
		
		if ( $persistence->validarExclusaoAssociado($id_associado) ){
		$msg_excessao = "Exclus�o n�o permitida";
		$_SESSION['msg_excessao'] = $msg_excessao;		
		header ("location: ../associado_lista.php");
		
		} else {		
			
		$persistence->excluirAssociado($id_associado,$opcao,$documemnto,$te_imagem);
	}
}

if ( isset($_GET['abrirAssociadoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		unset($_SESSION['opcao_ree']);
		
		$_SESSION['botao_ree'] = 0;
							
		header ("location: ../associado_lista.php");
}

if ( isset($_GET['imprimirAssociadoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);	
		
					
		header ("location: ../associado_lista_pdf.php");
}

if ( isset($_GET['abrirAniversarianteLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);	
		
							
		header ("location: ../aniversariantes_lista.php");
}

if ( isset($_GET['imprimirAniversarianteLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);	
		
							
		header ("location: ../aniversariantes_lista_pdf.php");
}

if ( isset($_GET['abrirAssociadoFicha']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
				
		$id_associado = addslashes($_GET['id_associado']);
		$_SESSION['id_associado'] = $id_associado;
											
		header ("location: ../associado_ficha.php");
}


if ( isset($_GET['abrirTorpedo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
							
		header ("location: ../enviar.php");
}

if ( isset($_POST['enviarTorpedo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$nu_zap = addslashes($_POST['nu_zap']);
		$nu_ori = "71988434464";
		$te_mensagem = addslashes($_POST['te_mensagem']);
		$te_mensagem = urlencode($te_mensagem);
		$te_mensagem = str_replace('+',' ',$te_mensagem);
		$numero = $nu_zap;
		$usuario = "jairo.sms";
		$senha = "cz4tweirzua";
		$id = "";
		$data = date("d-m-Y");
		
		if ( $te_mensagem == "" ){
		$msg_excessao = "Mensagem: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;		
		header ("location: ../enviar.php");
		
		} else {	
				
		
		$url = 'http://secure.talktelecom.com.br/api/EnvioSimples/EnviarJson';
		$data = array(
		'Usuario'		=> $usuario,
		'Senha'			=> $senha,
		'NA'			=> $nu_ori,
		'NB'			=> $nu_zap,
		'Mensagem'		=> $te_mensagem,
		'DataInicio'	=> "".date($data)."",
		'Id'			=> $id,
		'RespostaOpcao'	=> "1", //padrao
		);
	
		$data_string = json_encode($data);
		echo $url;
		echo $data_string;
		
		$ch = curl_init($url);
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
   		 curl_setopt($ch, CURLOPT_HEADER, true);
    	curl_setopt($ch, CURLOPT_HTTPHEADER,
               
			array('Content-Type:application/json',
                      'Content-Length: ' . strlen($data_string))
     		);

    	$result = curl_exec($ch);
    	curl_close($ch);
	
		echo $ch;
		
		// log ******************************************* 
		$dt_log = date("Y-m-d");
		$time = mktime(date('H')-3, date('i'), date('s'));
		$hora_local = gmdate("H:i:s", $time);
		$hora = substr($hora_local,0,2).substr($hora_local,2,3);
		$hr_log = $hora;
		$id_acao = 1;
		$nm_objeto = "SMS";
		$nu_ip = getenv("REMOTE_ADDR");
		
		$inserir = mysql_query("INSERT INTO logs (dt_log,hr_log,id_usuario,id_acao,nm_objeto,nu_ip) 
		VALUES('".$dt_log."','".$hr_log."',".$_SESSION['id_usuario'].",".$id_acao.",'".$nm_objeto."','".$nu_ip."')") or die("Erro 17.1");
	// ********************************************************************************************************************************

	
		$msg_sucesso = "Mensagem enviada com sucesso";	
		$_SESSION['msg_sucesso'] = $msg_sucesso;
	
		header ("location: ../enviar.php");
		
		}
}
?>