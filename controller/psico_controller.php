<?php
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();

if ( isset($_GET['abrirPsico']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_psico']);
		unset($_SESSION['nm_psico']);
		unset($_SESSION['hr_psico']);
		unset($_SESSION['id_associado']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['id_psicanalista']);				
		unset($_SESSION['nm_psicanalista']);				
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['id_tipo']);
		unset($_SESSION['opcao']);
		unset($_SESSION['nu_rg']);
		unset($_SESSION['nu_cpf']);
		unset($_SESSION['id_categoria']);
		unset($_SESSION['nm_categoria']);		
		
		$id_associado = addslashes($_GET['id_associado']);
							
		//header ("location: ../psico.php");
		$persistence->abrirPsico($id_associado);
}

if ( isset($_POST['inserirPsico']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);		
		
		$id_associado = addslashes($_POST['id_associado']);
		$nm_associado = addslashes($_POST['nm_associado']);
		$dt_inicio = addslashes($_POST['dt_inicio']);
		$dt_fim = addslashes($_POST['dt_fim']);
		$hr_psico = addslashes($_POST['hr_psico']);
		$id_psicanalista = addslashes($_POST['id_psicanalista']);
		$nm_psicanalista = addslashes($_POST['nm_psicanalista']);
		$id_categoria = addslashes($_POST['id_categoria']);
		$nm_categoria = addslashes($_POST['nm_categoria']);
		$te_obs = addslashes($_POST['te_obs']);
		
		if ( $dt_inicio == "" ){
		$msg_excessao = "Data In�cio: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;		
		header ("location: ../psico.php");
		
		} else if ( $dt_fim == "" ){
		$msg_excessao = "Data Fim: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;		
		header ("location: ../psico.php");
		
		} else if ( $hr_psico == "" ){
		$msg_excessao = "Hora: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;		
		header ("location: ../psico.php");
		
		} else if ( strlen( $hr_psico ) < 5 ){
		$msg_excessao = "Hora inv�lida";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;		
		header ("location: ../psico.php");
		
		} else if ( (substr($hr_psico,0,2) > 23) || (substr($hr_psico,3,2) > 59) ){ // 12:12
		$msg_excessao = "Hora: formato inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;		
		header ("location: ../psico.php");
		
		} else if ( $id_categoria == 0 ){
		$msg_excessao = "Categoria: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../psico.php");
		
		} else if ( $id_psicanalista == 0 ){
		$msg_excessao = "Profissional: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../psico.php");
		
		} else if ( $persistence->validarPsico($dt_inicio,$dt_fim,$id_associado) ){
		$msg_excessao = "Associado em Atendimento";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../psico.php");
		
		} else if ( $persistence->validarPsica($dt_inicio,$dt_fim,$id_psicanalista) ){
		$msg_excessao = "Profissional em Atendimento";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['nm_categoria'] = $nm_categoria;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../psico.php");	
		
		} else {
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);		
		unset($_SESSION['dt_inicio']);		
		unset($_SESSION['dt_fim']);		
		unset($_SESSION['hr_psico']);		
		unset($_SESSION['id_psicanalista']);
		unset($_SESSION['nm_psicanalista']);
		unset($_SESSION['id_categoria']);
		unset($_SESSION['nm_categoria']);				
		unset($_SESSION['te_obs']);				
		
		$persistence->inserirPsico($id_associado,$nm_associado,$dt_inicio,$dt_fim,$hr_psico,$id_psicanalista,$id_categoria,$te_obs);
}
}

if ( isset($_POST['editarPsico']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);			
		
		$id_associado = addslashes($_POST['id_associado']);
		$nm_associado = addslashes($_POST['nm_associado']);
		$dt_inicio = addslashes($_POST['dt_inicio']);
		$dt_fim = addslashes($_POST['dt_fim']);
		$hr_psico = addslashes($_POST['hr_psico']);
		$id_psicanalista = addslashes($_POST['id_psicanalista']);
		$nm_psicanalista = addslashes($_POST['nm_psicanalista']);
		$id_categoria = addslashes($_POST['id_categoria']);
		$nm_categoria = addslashes($_POST['nm_categoria']);
		$nm_psico = strtoupper($nm_psico);
		$te_obs = addslashes($_POST['te_obs']);
		$id_psico = addslashes($_POST['id_psico']);
		$opcao = addslashes($_POST['opcao']);
		
		if ( $dt_inicio == "" ){
		$msg_excessao = "Data In�cio: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['te_obs'] = $te_obs;		
		header ("location: ../psico_edit.php");
		
		} else if ( $dt_fim == "" ){
		$msg_excessao = "Data Fim: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['te_obs'] = $te_obs;		
		header ("location: ../psico_edit.php");
		
		} else if ( $hr_psico == "" ){
		$msg_excessao = "Hora: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['te_obs'] = $te_obs;		
		header ("location: ../psico_edit.php");
		
		} else if ( $id_psicanalista == 0 ){
		$msg_excessao = "Psicanalista: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../psico_edit.php");
		
		} else if ( $persistence->validarPsico($dt_inicio,$dt_fim,$id_associado) ){
		$msg_excessao = "Atendmento j� cadastrado para o associado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../psico_edit.php");
		
		} else if ( $persistence->validarPsica($dt_inicio,$dt_fim,$id_psicanalista) ){
		$msg_excessao = "Atendmento j� cadastrado para o(a) psicanalista";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_associado'] = $nm_associado;
		$_SESSION['dt_inicio'] = $dt_inicio;
		$_SESSION['dt_fim'] = $dt_fim;
		$_SESSION['hr_psico'] = $hr_psico;
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../psico_edit.php");	
		
		} else {
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);		
		unset($_SESSION['dt_inicio']);		
		unset($_SESSION['dt_fim']);		
		unset($_SESSION['hr_psico']);		
		unset($_SESSION['id_psicanalista']);
		unset($_SESSION['nm_psicanalista']);				
		unset($_SESSION['te_obs']);				
		
		$persistence->editarPsico($id_psico,$opcao,$id_associado,$nm_associado,$dt_inicio,$dt_fim,$hr_psico,$id_psicanalista,$id_categoria,$te_obs);
}
}

if ( isset($_GET['abrirPsicanaLookup']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_psicanalista']);
		unset($_SESSION['nm_psicanalista']);	
		unset($_SESSION['te_conselho']);
							
		header ("location: ../psicanalista_lookup.php");
		
	}

if ( isset($_GET['abrirPsicanalista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_psicanalista']);
		unset($_SESSION['nm_psicanalista']);	
		
							
		header ("location: ../psicanalista.php");
		
	}

if ( isset($_POST['lookupPsicanalista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_associado']);
		
		$te_conselho = trim(addslashes($_POST['te_conselho']));
		$te_conselho = strtoupper($te_conselho);
				
		if ( $te_conselho == ""){
		$msg_excessao = "Conselho: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		header ("location: ../psicanalista_lookup.php");
							
		} else if ( $persistence->lookupPsicanalista($te_conselho) ){
		$msg_excessao = "Conselho Regional j� cadastrado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['te_conselho'] = $te_conselho;		
		header ("location: ../psicanalista_lookup.php");
		
		} else {
		
		$_SESSION['te_conselho'] = $te_conselho;		
		header ("location: ../psicanalista.php");
	}
	}


if ( isset($_POST['inserirPsicanalista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$nm_psicanalista = trim(addslashes($_POST['nm_psicanalista']));
		$nm_psicanalista = strtoupper($nm_psicanalista);
		$id_sexo = addslashes($_POST['id_sexo']);
		$nm_sexo = addslashes($_POST['nm_sexo']);
		$te_conselho = trim(addslashes($_POST['te_conselho']));
		$te_conselho = strtoupper($te_conselho);
		$upload  = $_FILES['upload'];
		$te_imagem = addslashes($_POST['te_imagem']);
		
		$upextensao['extensoes'] = array('pdf','jpg');
		$extensao = strtolower(end(explode('.', $_FILES['upload']['name'])));
		$uptamanho['tamanho'] = 2097152; // 2Mb
		
		$imagem = $_FILES["upload"]["name"]; //pega o nome do arquivo
   		$temp_imagem = $_FILES["upload"]["tmp_name"]; //pega o "temp" do arquivo
   		$tipo = $_FILES["upload"]["type"]; //pega o tipo do arquivo
   		$tamanho = $_FILES["upload"]["size"]; //pega o tamanho do arquivo
   		$t_maximo = 2000000; //tamanho m�ximo do arquivo - em bytes
   		$uploaddir = '../fotos/';
		$uploadfile = $uploaddir . $_FILES['upload']['name'];				
		
		$imagem = isset($_FILES['upload']) ? $_FILES['upload'] : FALSE;
		
		if ($nm_psicanalista == ""){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ($id_sexo == 0){
		$msg_excessao = "Sexo: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ($te_conselho == ""){
		$msg_excessao = "Conselho: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ( $persistence->lookupPsicanalista($te_conselho) ){
		$msg_excessao = "Conselho Regional j� cadastrado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ($imagem == ""){
		$msg_excessao = "Escolha um arquivo � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ( ereg("[][><}{)(:;,!?*%&#@]", $imagem) ){
		$msg_excessao = "Foto cont�m caracteres inv�lidos";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "Foto: tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "Foto: tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ( ($te_imagem != "") && (!eregi("[gif|jpeg|jpg]", $tipo)) ){
		$msg_excessao = "Foto: Tipo de arquivo inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ( is_dir($imagem) ){
		$msg_excessao = "Foto: Selecione um <u>arquivo</u> � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
		
		} else if ( file_exists("$uploaddir"."$imagem") ){
		$msg_excessao = "Foto: J� existe um arquivo com este nome $imagem, por favor, renomeie-o";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista.php");
				
		} else {
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_psicanalista']);
		unset($_SESSION['id_sexo']);
		unset($_SESSION['nm_sexo']);
		unset($_SESSION['te_conselho']);
		
		move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile) ;
		
		$persistence->inserirPsicanalista($nm_psicanalista,$id_sexo,$te_conselho,$te_imagem);
}
}

if ( isset($_POST['editarPsicanalista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$nm_psicanalista = trim(addslashes($_POST['nm_psicanalista']));
		$nm_psicanalista = strtoupper($nm_psicanalista);
		$id_sexo = addslashes($_POST['id_sexo']);
		$nm_sexo = addslashes($_POST['nm_sexo']);
		$te_conselho = trim(addslashes($_POST['te_conselho']));
		$te_conselho = strtoupper($te_conselho);
		$upload  = $_FILES['upload'];
		$te_imagem = addslashes($_POST['te_imagem']);
		$te_imagem_arq = addslashes($_POST['te_imagem_arq']);
		$id_psicanalista = addslashes($_POST['id_psicanalista']);
		$opcao = addslashes($_POST['opcao']);
		$checkbox = addslashes($_POST['checkbox']);
		
		$upextensao['extensoes'] = array('pdf','jpg');
		$extensao = strtolower(end(explode('.', $_FILES['upload']['name'])));
		$uptamanho['tamanho'] = 2097152; // 2Mb
		
		$imagem = $_FILES["upload"]["name"]; //pega o nome do arquivo
   		$temp_imagem = $_FILES["upload"]["tmp_name"]; //pega o "temp" do arquivo
   		$tipo = $_FILES["upload"]["type"]; //pega o tipo do arquivo
   		$tamanho = $_FILES["upload"]["size"]; //pega o tamanho do arquivo
   		$t_maximo = 2000000; //tamanho m�ximo do arquivo - em bytes
   		$uploaddir = '../fotos/';
		$uploadfile = $uploaddir . $_FILES['upload']['name'];				
		
		$imagem = isset($_FILES['upload']) ? $_FILES['upload'] : FALSE;
		
		if ($nm_psicanalista == ""){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
		
		} else if ($id_sexo == 0){
		$msg_excessao = "Sexo: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
		
		} else if ($te_conselho == ""){
		$msg_excessao = "Conselho: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
				
		} else if ($imagem == ""){
		$msg_excessao = "Escolha um arquivo � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
		
		} else if ( ereg("[][><}{)(:;,!?*%&#@]", $imagem) ){
		$msg_excessao = "Foto cont�m caracteres inv�lidos";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
		
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "Foto: tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
		
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "Foto: tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
		
		} else if ( ($te_imagem != "") && (!eregi("[gif|jpeg|jpg]", $tipo)) ){
		$msg_excessao = "Foto: Tipo de arquivo inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
		
		} else if ( is_dir($imagem) ){
		$msg_excessao = "Foto: Selecione um <u>arquivo</u> � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
		
		} else if ( file_exists("$uploaddir"."$imagem") ){
		$msg_excessao = "Foto: J� existe um arquivo com este nome $imagem, por favor, renomeie-o";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_psicanalista'] = $nm_psicanalista;
		$_SESSION['id_sexo'] = $id_sexo;
		$_SESSION['nm_sexo'] = $nm_sexo;
		$_SESSION['te_conselho'] = $te_conselho;
		header ("location: ../psicanalista_edit.php");
				
		} else {
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_psicanalista']);
		unset($_SESSION['id_sexo']);
		unset($_SESSION['nm_sexo']);
		unset($_SESSION['te_conselho']);
		
		move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile) ;
		
		$persistence->editarPsicanalista($opcao,$id_psicanalista,$checkbox,$nm_psicanalista,$id_sexo,$te_conselho,$te_imagem,$te_imagem_arq);
}
}


if ( isset($_GET['abrirPsicanalistaEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_psico']);
				
		
		$id_psicanalista = addslashes($_GET['id_psicanalista']);
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		$_SESSION['opcao'] = $opcao;
							
		header ("location: ../psicanalista_edit.php");
}

if ( isset($_GET['excluirPsicanalista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_curriculo']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nu_cpf']);
		
		/* Diretorio que deve ser lido */

    $dir = '../fotos/';
	
    /* Abre o diret�rio */

    $pasta= opendir($dir);

    /* Loop para ler os arquivos do diretorio */
	
    while ($arquivo = readdir($pasta)){
	$itens[] = $arquivo;
	}
    /* Verificacao para exibir apenas os arquivos e nao os caminhos para diretorios superiores */
	sort($itens);
	foreach ($itens as $listar) {
    if ($listar != '.' && $listar != '..'){

    /* Escreve o nome do arquivo na tela */
	
    //echo "<a href='".$dir.$listar."'>".$listar."</a><a href='usuario_controller.php?excluirArquivo=excluirArquivo&arquivo=".$dir.$listar."')'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Excluir</a><br />";
	$arquivo = $dir.$listar;
    }
}		
		
		$id_psicanalista = addslashes($_GET['id_psicanalista']);
		$_SESSION['id_psicanalista'] = $id_psicanalista;
		
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['opcao'] = $opcao;
		
		if ( $persistence->validarExclucaoPsicanalista($id_psicanalista) ){
		$msg_excessao = "Exclus�o n�o permitida";
		$_SESSION['msg_excessao'] = $msg_excessao;
		
		header ("location: ../psicanalista_lista.php");
		
		} else {
			
			
		$persistence->excluirPsicanalista($id_psicanalista,$opcao,$arquivo);
	}
}
if ( isset($_GET['abrirPsicoEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_psico']);
				
		
		$id_psico = addslashes($_GET['id_psico']);
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['id_psico'] = $id_psico;
		$_SESSION['opcao'] = $opcao;
							
		header ("location: ../psico_edit.php");
}

if ( isset($_GET['abrirPsicoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
		$_SESSION['botao_ree'] = 0;
							
		header ("location: ../psico_lista.php");
}

if ( isset($_GET['imprimirPsicoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		
							
		header ("location: ../psico_lista_pdf.php");
}

if ( isset($_GET['abrirPsicanalistaLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
							
		header ("location: ../psicanalista_lista.php");
}

if ( isset($_GET['imprimirProfissionalLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		
							
		header ("location: ../psicanalista_lista_pdf.php");
}


if ( isset($_GET['excluirPsico']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);	
		
		$id_psico = addslashes($_GET['id_psico']);		
		$opcao = addslashes($_GET['opcao']);
		
		$persistence->excluirPsico($id_psico,$opcao);
}

if ( isset($_GET['abrirCategoria']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_psico']);
		unset($_SESSION['nm_psico']);
		unset($_SESSION['id_curso']);
		unset($_SESSION['nm_curso']);		
		unset($_SESSION['hr_psico']);
		unset($_SESSION['id_associado']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['id_psicanalista']);				
		unset($_SESSION['nm_psicanalista']);
		unset($_SESSION['id_categoria']);				
		unset($_SESSION['nm_categoria']);				
		unset($_SESSION['dt_inicio']);
		unset($_SESSION['dt_fim']);
		unset($_SESSION['nu_ano']);
		unset($_SESSION['id_tipo']);
		unset($_SESSION['opcao']);
		unset($_SESSION['nu_rg']);
		unset($_SESSION['nu_cpf']);		
		
		header ("location: ../categoria.php");
	}
	
if ( isset($_POST['inserirCategoria']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_curso']);
		unset($_SESSION['nm_curso']);
		unset($_SESSION['nu_carga']);
		
		$nm_categoria = trim(addslashes($_POST['nm_categoria']));
		$nm_categoria = strtoupper($nm_categoria);
				
		if ( $nm_categoria == "" ){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_categoria'] = $nm_categoria;
		
		header ("location: ../categoria.php");
						
		} else {	
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_categoria']);
				
		$persistence->inserirCategoria($nm_categoria);
	}
}

if ( isset($_POST['editarCategoria']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_curso']);
		unset($_SESSION['nm_curso']);
		unset($_SESSION['nu_carga']);
		
		$nm_categoria = trim(addslashes($_POST['nm_categoria']));
		$nm_categoria = strtoupper($nm_categoria);
		$opcao = addslashes($_POST['opcao']);
		$id_categoria = addslashes($_POST['id_categoria']);
				
		if ( $nm_categoria == "" ){
		$msg_excessao = "Nome: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_categoria'] = $nm_categoria;
		
		header ("location: ../categoria_edit.php");
						
		} else {	
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_categoria']);
				
		$persistence->editarCategoria($id_categoria,$nm_categoria,$opcao);
	}
}

if ( isset($_GET['abrirCategoriaLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
							
		header ("location: ../categoria_lista.php");
}

if ( isset($_GET['imprimirCategoriaLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);		
		
							
		header ("location: ../categoria_lista_pdf.php");
}


if ( isset($_GET['abrirCategoriaEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_categoria']);
				
		
		$id_categoria = addslashes($_GET['id_categoria']);
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['id_categoria'] = $id_categoria;
		$_SESSION['opcao'] = $opcao;
							
		header ("location: ../categoria_edit.php");
}


if ( isset($_GET['excluirCategoria']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
						
		$id_categoria = addslashes($_GET['id_categoria']);
		$opcao = addslashes($_GET['opcao']);
		
		if ( $persistence->validarExclusaoCategoria($id_categoria) ){
		$msg_excessao = "Exclus�o n�o permitida";
		$_SESSION['msg_excessao'] = $msg_excessao;
		
		header ("location: ../categoria_lista.php");
		
		} else {
									
		$persistence->excluirCategoria($id_categoria,$opcao);
}
}


?>