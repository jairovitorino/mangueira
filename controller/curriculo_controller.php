<?php 
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();

	
if ( isset($_GET['abrirCurriculo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_curriculo']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nu_cpf']);
		unset($_SESSION['nu_telefone']);
		unset($_SESSION['dt_nascimento']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_profissao_like']);
		unset($_SESSION['nm_indicacao']);
				
		unset($_SESSION['nm_logra']);
		unset($_SESSION['nu_logra']);				
		unset($_SESSION['te_email']);
		unset($_SESSION['nm_compl']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nu_cep']);
		unset($_SESSION['nm_cidade']);
		unset($_SESSION['nm_uf']);
		unset($_SESSION['opcao']);
		unset($_SESSION['te_obs']);
		
		$id_associado = addslashes($_GET['id_associado']);		
		
		$persistence->abrirCurriculo($id_associado);
		//header ("location: ../curriculo.php");
	}

if ( isset($_POST['lookupCurriculo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_curriculo']);
		
		$nu_cpf = addslashes($_POST['nu_cpf']);
		//$nu_cpf = intval($nu_cpf);
		$checkbox = addslashes($_POST['checkbox']);
		
		if ( (!$checkbox) && ($nu_cpf == "") ){
		$msg_excessao = "CPF: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		header ("location: ../curriculo_lookup.php");
		
		} else if ( ($nu_cpf != "" ) && (strlen( $nu_cpf ) != 11)) {
		$msg_excessao = "CPF: Preenchimento inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		header ("location: ../curriculo_lookup.php");
		
		} else if (($nu_cpf != "") && (!is_numeric($nu_cpf))){
		$msg_excessao = "CPF: caracteres n�o aceito";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;
		header ("location: ../curriculo_lookup.php");
					
		} else if ( $persistence->lookupCurriculo($nu_cpf) ){
		$msg_excessao = "CPF j� cadastrado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;		
		header ("location: ../curriculo_lookup.php");
		
		} else if ( ($nu_cpf != "") && (!$persistence->validCPF($nu_cpf)) ){
		$msg_excessao = "CPF: d�gito inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nu_cpf'] = $nu_cpf;		
		header ("location: ../curriculo_lookup.php");
		
		} else {
		
		$_SESSION['nu_cpf'] = $nu_cpf;
		$_SESSION['checkbox'] = $checkbox;
		header ("location: ../curriculo.php");
	}
	}
	
	if ( isset($_POST['inserirCurriculo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$id_associado = addslashes($_POST['id_associado']);
		$nm_associado = addslashes($_POST['nm_associado']);		
		$nm_profissao = trim(addslashes($_POST['nm_profissao']));
		$nm_profissao = strtoupper($nm_profissao);
		$nm_indicacao = trim(addslashes($_POST['nm_indicacao']));
		$nm_indicacao = strtoupper($nm_indicacao);		
		$te_obs =addslashes($_POST['te_obs']);		
		$te_arquivo = addslashes($_POST['te_arquivo']);
		$te_arquivo = substr($te_arquivo,14);
		$te_arquivo = str_replace("�","C",$te_arquivo);
		$upload = addslashes($_POST['upload']);
								
		$upextensao['extensoes'] = array('pdf','jpg');
		$extensao = strtolower(end(explode('.', $_FILES['upload']['name'])));
		$uptamanho['tamanho'] = 2097152; // 2Mb
		
		$imagem = $_FILES["upload"]["name"]; //pega o nome do arquivo
   		$temp_imagem = $_FILES["upload"]["tmp_name"]; //pega o "temp" do arquivo
   		$tipo = $_FILES["upload"]["type"]; //pega o tipo do arquivo
   		$tamanho = $_FILES["upload"]["size"]; //pega o tamanho do arquivo
   		$t_maximo = 5000000; //tamanho m�ximo do arquivo - em bytes
   		$uploaddir = '../curriculos/';
		$uploadfile = $uploaddir . $_FILES['upload']['name'];
				
		
		$imagem = isset($_FILES['upload']) ? $_FILES['upload'] : FALSE;
		
		if ( $nm_profissao == "" ){
		$msg_excessao = "Profiss�o: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
		
		} else if ( $te_arquivo == "" ){
		$msg_excessao = "Curriculo: Preenchimento obrigat�rio";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
		
		} else if ($imagem == ""){
		$msg_excessao = "Escolha um arquivo � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
		
		} else if ( ereg("[][><}{)(:;,!?*%&#@]", $imagem) ){
		$msg_excessao = "O nome do arquivo cont�m caracteres inv�lidos";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
				
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "O tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
		
		} else if ( $tamanho > $t_maximo ){
		$msg_excessao = "O tamanho m�ximo permitido � de 2MB";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
		
		} else if ( ($te_imagem != "") && (!eregi("[gif|jpeg|jpg]", $tipo)) ){
		$msg_excessao = "Tipo de arquivo inv�lido";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
		
		} else if ( is_dir($imagem) ){
		$msg_excessao = "Selecione um <u>arquivo</u> � ser enviado";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
		
		/*} else if ( file_exists("$uploaddir"."$imagem") ){
		$msg_excessao = "J� existe um arquivo com este nome $imagem, por favor, renomeie-o";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");*/
		
		} else if ( ($imagem != "") && (file_exists("$uploaddir"."$imagem")) ){	
		$msg_excessao = "J� existe um arquivo com este nome $imagem, por favor, renomeie-o";
		$_SESSION['msg_excessao'] = $msg_excessao;
		$_SESSION['nm_profissao'] = $nm_profissao;
		$_SESSION['te_obs'] = $te_obs;
		header ("location: ../curriculo.php");
		
		
		} else {
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['te_obs']);
		
		move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile) ;
		
		$persistence->inserirCurriculo($id_associado,$nm_profissao,$nm_indicacao,$te_obs,$te_arquivo);
		}
}

if ( isset($_GET['abrirCurriculoEdit']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nu_cpf']);
		
				
		$id_curriculo = addslashes($_GET['id_curriculo']);
		$_SESSION['id_curriculo'] = $id_curriculo;
		
		$opcao = addslashes($_GET['opcao']);
		$_SESSION['opcao'] = $opcao;
		
		
		header ("location: ../curriculo_edit.php");
	}
	
if ( isset($_POST['editarCurriculo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		$id_associado = addslashes($_POST['id_associado']);
		$nm_associado = addslashes($_POST['nm_associado']);
		$nm_profissao = trim(addslashes($_POST['nm_profissao']));
		$nm_profissao = strtoupper($nm_profissao);
		$nm_indicacao = trim(addslashes($_POST['nm_indicacao']));
		$nm_indicacao = strtoupper($nm_indicacao);		
		$dt_curriculo = addslashes($_POST['dt_curriculo']);
		$nm_instituicao = trim(addslashes($_POST['nm_instituicao']));
		$nm_instituicao = strtoupper($nm_instituicao);
		$te_obs =addslashes($_POST['te_obs']);		
		$te_arquivo = addslashes($_POST['te_arquivo']);
		$te_arquivo = substr($te_arquivo,15);
		$te_arquivo = str_replace("�","C",$te_arquivo);
		$te_arquivo_arq = addslashes($_POST['te_arquivo_arq']);
		$upload = addslashes($_POST['upload']);
		$upload = str_replace("�","C",$upload);
		$st_curriculo = addslashes($_POST['st_curriculo']);		
		$id_curriculo = addslashes($_POST['id_curriculo']);
		$opcao = addslashes($_POST['opcao']);
		$checkbox = addslashes($_POST['checkbox']);
		
		$upextensao['extensoes'] = array('pdf','jpg');
		$extensao = strtolower(end(explode('.', $_FILES['upload']['name'])));
		$uptamanho['tamanho'] = 2097152; // 2Mb
		
		$imagem = $_FILES["upload"]["name"]; //pega o nome do arquivo
   		$temp_imagem = $_FILES["upload"]["tmp_name"]; //pega o "temp" do arquivo
   		$tipo = $_FILES["upload"]["type"]; //pega o tipo do arquivo
   		$tamanho = $_FILES["upload"]["size"]; //pega o tamanho do arquivo
   		$t_maximo = 5000000; //tamanho m�ximo do arquivo - em bytes
   		$uploaddir = '../curriculos/';
		$uploadfile = $uploaddir . $_FILES['upload']['name'];
		
		$imagem = isset($_FILES['upload']) ? $_FILES['upload'] : FALSE;		
		
		move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile) ;
		
		$persistence->editarCurriculo($checkbox,$opcao,$id_curriculo,$nm_profissao,$nm_indicacao,$te_obs,$te_arquivo,$te_arquivo_arq,$st_curriculo,$dt_curriculo,$nm_instituicao);
		
	}
	
if ( isset($_GET['abrirCurriculoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_associado_like']);
		unset($_SESSION['nm_indicacao']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
		unset($_SESSION['st_curriculo']);
		unset($_SESSION['nu_idade']);
		
		$st_curriculo = addslashes($_GET['st_curriculo']);
		$_SESSION['st_curriculo'] = $st_curriculo;
		$_SESSION['botao_ree'] = 0;
					
		header ("location: ../curriculo_lista.php");
}

if ( isset($_GET['imprimirCurriculoLista']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
				
							
		header ("location: ../curriculo_lista_pdf.php");
}

if ( isset($_GET['abrirArquivo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['nm_profissao']);
		unset($_SESSION['nm_associado']);
		unset($_SESSION['nm_indicacao']);
		unset($_SESSION['nm_bairro']);
		unset($_SESSION['nm_logra']);
		unset($_SESSION['st_curriculo']);
		unset($_SESSION['nu_idade']);
		
		$te_arquivo = addslashes($_GET['te_arquivo']);
			
		$opcao = addslashes($_GET['opcao']);
							
		
		header ("location: ../curriculos/".$te_arquivo);
}

if ( isset($_GET['excluirArquivo']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		unset($_SESSION['id_curriculo']);
		unset($_SESSION['nm_curriculo']);
		unset($_SESSION['nu_cpf']);		
		
		$id_curriculo = addslashes($_GET['id_curriculo']);		
		$opcao = addslashes($_GET['opcao']);
		$te_arquivo = addslashes($_GET['te_arquivo']);
			
		$doc = $te_arquivo;		
		/* Diretorio que deve ser lido */
    	$dir = '../curriculos/';   	 
		$documemnto = $dir.$doc; 
		
		$persistence->excluirArquivo($id_curriculo,$opcao,$documemnto);
	}


?>