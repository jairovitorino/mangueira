<?php
session_start();
require_once("../class/persistence.php");
$persistence = new Persistence();
			
	
if ( isset($_GET['abrirPontoManual']) )  {
	unset($_SESSION['msg_sucesso']);
	unset($_SESSION['msg_excessao']);
	unset($_SESSION['id_acao']);
		
	
	header ("location: ../folha_ponto.php");
	}
	
if ( isset($_GET['abrirPontoEletronico']) )  {
	unset($_SESSION['msg_sucesso']);
	unset($_SESSION['msg_excessao']);
			
	
	$persistence->abrirPontoEletronico();
	}

if ( isset($_POST['inserirPontoEletronico']) )  {
	unset($_SESSION['msg_sucesso']);
	unset($_SESSION['msg_excessao']);
			
	$id_ponto = addslashes($_POST['id_ponto']);
	$hr_ponto = addslashes($_POST['hr_ponto']);
	
	unset($_SESSION['hr_ponto']);
	$persistence->inserirPontoEletronico($id_ponto,$hr_ponto);
	}
	
if ( isset($_GET['excluirPonto']) ) {
				
		unset($_SESSION['msg_sucesso']);
		unset($_SESSION['msg_excessao']);
		
		
		$id_ponto_det = addslashes($_GET['id_ponto_det']);
		$opcao = addslashes($_GET['opcao']);
							
		$persistence->excluirPonto($id_ponto_det,$opcao);
}

?>