<?php
session_start();

//CONECTA AO MYSQL              
require_once("class/conexao.php");
$mysql = new Mysql();
$mysql->conectar(); 

define("FPDF_FONTPATH","fpdf/font/");
require_once("fpdf/fpdf.php");
$pdf = new FPDF('P'); 
$pdf->Open(); 

$pdf->AddPage(); 
// muda fonte e coloca em negrito

	$rs = mysql_query("SELECT * FROM associados ORDER BY nm_associado ");	
		
$pdf->SetFont('Arial', 'B', 7);

// largura padr�o das colunas
$largura = 45;
// altura padr�o das linhas das colunas
$altura = 6;

// criando os cabe�alhos para 5 colunas
$pdf->Cell($largura, $altura, 'Nome', 1, 0, 'L');
$pdf->Cell($largura, $altura, 'Telefone', 1, 0, 'L');
$pdf->Cell($largura, $altura, 'CPF', 1, 0, 'L');

// pulando a linha
$pdf->Ln($altura);

// tirando o negrito
$pdf->SetFont('Arial', '', 7);

// montando a tabela com os dados (presumindo que a consulta j� foi feita)
while( $row = mysql_fetch_assoc($rs) )
{
	$pdf->Cell($largura, $altura, $row['nm_associado'], 1, 0, 'L');
	$pdf->Cell($largura, $altura, $row['nu_telefone'], 1, 0, 'L');
	$pdf->Cell($largura, $altura, $row['nu_cpf'], 1, 0, 'L');
	
	$pdf->Ln($altura);
}

// exibindo o PDF
$pdf->Output();
?>