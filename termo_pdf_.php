<?php
session_start();

//CONECTA AO MYSQL              
require_once("class/conexao.php");
$mysql = new Mysql();
$mysql->conectar(); 


define("FPDF_FONTPATH","fpdf/font/");
require_once("fpdf/fpdf.php");
$pdf = new FPDF('P'); 
$pdf->Open(); 

$pdf->AddPage(); 

$pdf->Image('img/logo_projeto.jpg',96,9,25,20);

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(92, 33);
$texto = "PROJETO MANGUEIRA";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 7);
$pdf->SetXY(85, 39);
$texto = "BLOCO INFANTIL OS MANGUEIRINHAS";
$pdf->Cell(0,0.5,$texto, 4, 'J');

// 1A LINHA HORIZONTAL
$pdf->SetXY(20,46);
$pdf->Cell(0,0,'',1,1,'L');

// 2A LINHA HORIZONTAL
$pdf->SetXY(20,87);
$pdf->Cell(0,0,'',1,1,'L');

$pdf->SetFont('Arial', 'B', 6);
$pdf->SetXY(20, 49);
$texto = "DADOS RESPONS�VEL ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 6);
$pdf->SetXY(20, 89);
$texto = "ASSOCIADO(S) ";
$pdf->Cell(0,0.5,$texto, 4, 'J');


$id_bloco = $_SESSION['id_bloco'];
$nu_ano_sis = date("Y");

$sql = mysql_query("SELECT * FROM associados, blocos 
WHERE blocos.id_associado = associados.id_associado 
AND blocos.nu_ano = ".$nu_ano_sis." 
AND id_bloco = ".$id_bloco." ");
$row = mysql_num_rows($sql);

for($i=0; $i<$row; $i++) {
	$id_associado = mysql_result($sql, $i, "id_associado");
	$nm_associado = mysql_result($sql, $i, "nm_associado");
	$nu_rg = mysql_result($sql, $i, "nu_rg");
	$nu_cpf = mysql_result($sql, $i, "nu_cpf");
	$dt_nascimento = mysql_result($sql, $i, "dt_nascimento");
	$dt_nascimento = substr($dt_nascimento,8,2)."/".substr($dt_nascimento,5,2)."/".substr($dt_nascimento,0,4);
	$ck_trabalho = mysql_result($sql, $i, "ck_trabalho");
	$ck_trabalho == 0 ? $ck_trabalho = "N�o" : $ck_trabalho = "Sim";
	$st_responsavel = mysql_result($sql, $i, "st_responsavel");
	$nm_logra = mysql_result($sql, $i, "nm_logra");	
	$nu_logra = mysql_result($sql, $i, "nu_logra");
	$nm_compl = mysql_result($sql, $i, "nm_compl");
	$nm_bairro = mysql_result($sql, $i, "nm_bairro");	
	$nu_cep = mysql_result($sql, $i, "nu_cep");
	$nu_telefone = mysql_result($sql, $i, "nu_telefone");	
	$nm_cidade = mysql_result($sql, $i, "nm_cidade");
	$nm_uf = mysql_result($sql, $i, "nm_uf");
	$nu_ano = mysql_result($sql, $i, "nu_ano");
	
	
	switch($st_responsavel){
	case 1;
	$st_responsavel = "M�e";
	break;
	case 2;
	$st_responsavel = "Pai";
	break;
	case 3;
	$st_responsavel = "Outro";
	break;		
	}	
	
}
 
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(20, 55);
$texto = "Nome: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(31, 55);
$texto = $nm_associado;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(130, 55);
$texto = "CPF: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(141, 55);
$texto = $nu_cpf;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(20, 62);
$texto = "RG: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(26, 62);
$texto = $nu_rg;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(50, 62);
$texto = "Data de Nascimento: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(79, 62);
$texto = $dt_nascimento;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(97, 62);
$texto = "Trabalha: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(111, 62);
$texto = $ck_trabalho;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(121, 62);
$texto = "Rela��o: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(135, 62);
$texto = $st_responsavel;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(145, 62);
$texto = "Telefone: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(160, 62);
$texto = $nu_telefone;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(20, 69);
$texto = "Logradouro: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(38, 69);
$texto = $nm_logra;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(120, 69);
$texto = "N�mero: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(135, 69);
$texto = $nu_logra;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(140, 69);
$texto = "Complemento: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(161, 69);
$texto = $nm_compl;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(20, 76);
$texto = "CEP: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(28, 76);
$texto = $nu_cep;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(45, 76);
$texto = "Bairro: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(56, 76);
$texto = $nm_bairro;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(90, 76);
$texto = "Cidade: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(102, 76);
$texto = $nm_cidade;
$pdf->Cell(0,0.5,$texto, 4, 'J');


$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(140, 76);
$texto = "UF: ";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(150, 76);
$texto = $nm_uf;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(20, 95);
$texto = "OR";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(30, 95);
$texto = "FOLI�O";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', '', 7);
$pdf->SetXY(89, 95);
$texto = "RG";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(110, 95);
$texto = "SEXO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(120, 95);
$texto = "PARENTESCO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(144, 95);
$texto = "NASCIMENTO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(165, 95);
$texto = "ANO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$sql1 = mysql_query("SELECT dt_nascimento,blocos.dt_nascimento,(YEAR(CURDATE())-YEAR(blocos.dt_nascimento)) AS nu_idade,nm_foliao,nu_rg_foliao,id_sexo,st_parentesco,st_responsavel,nu_ano
FROM blocos WHERE id_associado = ".$id_associado." ");
$j = 1;
while ( $vetor = mysql_fetch_array($sql1) ){
$vetor['id_sexo'] == 1 ? $nm_sexo = "M" : $nm_sexo = "F";
$vetor['st_parentesco'] == 1 ? $st_parentesco = "Filho(a)" : $st_parentesco = "Outro";
$dt_nascimento = substr($vetor['dt_nascimento'],8,2)."/".substr($vetor['dt_nascimento'],5,2)."/".substr($vetor['dt_nascimento'],0,4);
$dt_nascimento == "00/00/0000" ? $dt_nascimento = "" : $dt_nascimento = $dt_nascimento;

switch($vetor['st_responsavel']){
case 1;
$st_responsavel = "M�e";
break;
case 2;
$st_responsavel = "Pai";
break;
case 3;
$st_responsavel = "Outro";
break;
}
$pdf->Ln();
$pdf->SetX(20);
$pdf->Cell(0,5,$j);
$pdf->SetX(30);
$pdf->Cell(0,5,$vetor['nm_foliao']);
$pdf->SetX(89);
$pdf->Cell(0,5,$vetor['nu_rg_foliao']);
$pdf->SetX(110);
$pdf->Cell(0,5,$nm_sexo);
$pdf->SetX(120);
$pdf->Cell(0,5,$st_parentesco);
$pdf->SetX(144);
$pdf->Cell(0,5,$dt_nascimento);
$pdf->SetX(165);
$pdf->Cell(0,5,$vetor['nu_ano']);
$j = $j + 1;
}

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(80, 125);
$texto = "TERMO DE RESPONSABILIDADE";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(25, 132);
$texto = "Pelo presente TERMO DE RESPONSABILIDADE, Eu ".$nm_associado." acima identificado(a) autorizo e assumo a inteira e toral responsabilidade pela participa��o do(s) menor(es) associado(a)(s) supracitado(s) no desfile do Bloco Infantil OS MANGUEIRINHAS ".$nu_ano.", e comprometo-me a zelar pela sua seguran�a, guarda, sa�de, respeito, dignidade, moralidade e demais direitos, inerentes a crian�as e o adolescente, estando em sua companhia, pelo per�odo em que o bloco infantil realizar o seu desfile no circuito do Carnaval da Liberdade. Do mesmo modo, autorizo expressamente a utiliza��o da minha imagem e voz, assim como a imagem e voz da(s) crian�a(s) aqui relaciona(s), em car�ter definitivo e gratuito, constante em fotos e filmagens decorrentes da participa��o no desfile do Bloco Infantil OS MANGUERINHAS ".$nu_ano.". Pelo presente termo, reconhe�o que o Bloco Infantil OS MANGUEIRINHAS � um bloco tradicional n�o oneroso para os seus foli�es. Estabelece que qualquer acidente provocado por terceiros, ou anormmalidade que cause prejuizo ou dano, ao respons�vel bem como a(s) crian�a(s) aqui relacionada(s), assim como decorrentes da n�o observ�ncia do respons�vel para com a seguran�a, guarda, zelo, sa�de e moralidade da(s) referida(s) crian�a(s) durante o desfile do Bloco Infantil OS MANGUEIRINHAS ".$nu_ano.", no circuito do carnaval da Liberdade, ser� de total e integral responsabilidade dos pais e/ou respons�veis pela crian�a, isentando automaticamente a entidade carnavalesca Bloco Infantil OS MANGUEIRINHAS ".$nu_ano.", seus patrocinadores, a Associa��o Comunit�ria Amparo Social e Cultural do Bairro da Liberdade Projeto Mangueira, assim como a diretoria, coordena��o e colaboradores de qualquer responsabilidade civil, criminal ou administrativa. E por est� de acordo assino abaixo. ";
$pdf->MultiCell(0,5,$texto, 4, 'J');

$data = date("d/m/Y");

$pdf->SetXY(25, 218);
$texto = "DATA ".$data;;
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(40, 248);
$texto = "---------------------------------------------";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(50, 250);
$texto = "Ass. da Diretoria";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(130, 248);
$texto = "---------------------------------------------";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(140, 250);
$texto = "Ass. do Respons�vel";
$pdf->Cell(0,0.5,$texto, 4, 'J');


$pdf->Output();
?>