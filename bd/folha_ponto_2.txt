<?php
session_start();

//CONECTA AO MYSQL              
require_once("class/conexao.php");
$mysql = new Mysql();
$mysql->conectar(); 

$ano = date("Y");
$mes = date("m");

switch($mes){
case 1;
$nm_mes = "Janeiro";
$nu_mes = 31;
break;
case 2;
$nm_mes = "Fevereiro";
$nu_mes = 29;
break;
case 3;
$nm_mes = "Mar�o";
$nu_mes = 31;
break;
case 4;
$nm_mes = "Abril";
$nu_mes = 30;
break;
case 5;
$nm_mes = "Maio";
$nu_mes = 31;
break;
case 6;
$nm_mes = "Junho";
$nu_mes = 30;
break;
case 7;
$nm_mes = "Julho";
$nu_mes = 31;
break;
case 8;
$nm_mes = "Agosto";
$nu_mes = 31;
break;
case 9;
$nm_mes = "Setembro";
$nu_mes = 30;
break;
case 10;
$nm_mes = "Outubro";
$nu_mes = 31;
break;
case 11;
$nm_mes = "Novembro";
$nu_mes = 30;
break;
case 12;
$nm_mes = "Dezembro";
$nu_mes = 31;
break;
}

define("FPDF_FONTPATH","fpdf/font/");
require_once("fpdf/fpdf.php");
$pdf = new FPDF('P'); 
$pdf->Open(); 

$pdf->AddPage(); 

$pdf->Image('img/logo_projeto.jpg',96,9,25,20);

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(92, 33);
$texto = "PROJETO MANGUEIRA";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetFont('Arial', 'B', 8);
$pdf->SetXY(95, 44);
$texto = "FOLHA DE PONTO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(39, 52);
$texto = "Mes: ".$nm_mes."/".$ano;
$pdf->Cell(0,0.5,$texto, 4, 'J');

// largura padr�o das colunas
$largura = 12;
// largura padr�o das colunas
$largura1 = 100;
// largura padr�o das colunas
$largura2 = 21;
// altura padr�o das linhas das colunas
$altura = 6;

$pdf->SetFont('Arial', 'B', 9);
$pdf->SetXY(30, 58);
$pdf->Cell($largura2, $altura, 'Dia', 1, 0, 'L');
$pdf->SetXY(51, 58);
$pdf->Cell($largura, $altura, 'Entr', 1, 0, 'L');
$pdf->SetXY(63, 58);
$pdf->Cell($largura, $altura, 'Saida', 1, 0, 'L');
$pdf->SetXY(75, 58);
$pdf->Cell($largura, $altura, 'Entr', 1, 0, 'L');
$pdf->SetXY(87, 58);
$pdf->Cell($largura, $altura, 'Saida', 1, 0, 'L');
$pdf->SetXY(99, 58);
$pdf->Cell($largura1, $altura, 'Obs', 1, 0, 'L');

/* example 1 */

 $diasemana = array('Domingo', 'Segunda', 'Ter�a', 'Quarta', 'Quinta', 'Sexta', 'Sabado');

 // Aqui podemos usar a data atual ou qualquer outra data no formato Ano-m�s-dia (2014-02-28)
 $data = date('Y-m-d');

 // Varivel que recebe o dia da semana (0 = Domingo, 1 = Segunda ...)
 $diasemana_numero = date('w', strtotime($data));

//( date( 'w' ) % 6 ) == 0 
$i = 1;
$pdf->SetFont('Arial', '', 9);

while ($i <= $nu_mes) {

$diasemana = array('Domingo', 'Segunda', 'Ter�a', 'Quarta', 'Quinta', 'Sexta', 'Sabado');

 // Aqui podemos usar a data atual ou qualquer outra data no formato Ano-m�s-dia (2014-02-28)
 //$data = date('Y-m-'.$i);
 
 $data = date('Y-m-'.$i); 

 // Varivel que recebe o dia da semana (0 = Domingo, 1 = Segunda ...)
 $diasemana_numero = date('w', strtotime($data));
 // $diasemana_numero == 0 ? $diasemana_numero = "domingo" : $diasemana_numero = $diasemana_numero;
 if ( $diasemana_numero == 0 ){
 $diasemana_numero = "domingo";
 
 } else if ( $diasemana_numero == 6 ){
 $diasemana_numero = "sabado";
 
 } else {
 $diasemana_numero = $i;
 }
 
$pdf->Ln($altura);
$pdf->SetX(30);
$pdf->Cell($largura2, $altura, $diasemana_numero, 1, 0, 'L');
$pdf->SetX(51);
$pdf->Cell($largura, $altura, '', 1, 0, 'L');
$pdf->SetX(63);
$pdf->Cell($largura, $altura, '', 1, 0, 'L');
$pdf->SetX(75);
$pdf->Cell($largura, $altura, '', 1, 0, 'L');
$pdf->SetX(87);
$pdf->Cell($largura, $altura, '', 1, 0, 'L');
$pdf->SetX(99);
$pdf->Cell($largura1, $altura, '', 1, 0, 'L');
 
 $i++;
}

$pdf->SetXY(39, 260);
$texto = "-----------------------------------------------";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(49, 263);
$texto = "FUNCIONARIO";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(150, 260);
$texto = "-----------------------------------------------";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->SetXY(163, 263);
$texto = "GERENTE";
$pdf->Cell(0,0.5,$texto, 4, 'J');

$pdf->Output();
?>