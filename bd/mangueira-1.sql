﻿-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 14/11/2018 às 17h30min
-- Versão do Servidor: 5.5.16
-- Versão do PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `mangueira`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `associados`
--

CREATE TABLE IF NOT EXISTS `associados` (
  `id_associado` int(11) NOT NULL AUTO_INCREMENT,
  `nm_associado` varchar(30) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `id_escolaridade` int(11) NOT NULL,
  `nu_rg` varchar(10) NOT NULL,
  `dt_nascimento` date NOT NULL,
  `nu_telefone` varchar(30) NOT NULL,
  `nm_logra` varchar(40) NOT NULL,
  `nu_logra` varchar(6) NOT NULL,
  `nm_compl` varchar(15) NOT NULL,
  `nm_bairro` varchar(20) NOT NULL,
  `nu_cep` varchar(8) NOT NULL,
  `nm_cidade` varchar(20) NOT NULL,
  `nm_uf` varchar(2) NOT NULL,
  `te_obs` varchar(250) NOT NULL,
  `te_imagem` varchar(30) NOT NULL,
  `st_controle` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_associado`),
  KEY `id_escolaridade` (`id_escolaridade`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `associados`
--

INSERT INTO `associados` (`id_associado`, `nm_associado`, `id_sexo`, `id_escolaridade`, `nu_rg`, `dt_nascimento`, `nu_telefone`, `nm_logra`, `nu_logra`, `nm_compl`, `nm_bairro`, `nu_cep`, `nm_cidade`, `nm_uf`, `te_obs`, `te_imagem`, `st_controle`, `id_usuario`) VALUES
(9, 'GORETE ALMEIDA DE SANTANA', 2, 0, '123123111', '1978-06-10', '999387669', 'RUA JOAQUIM NAMBUCO DE ALMEIDA', '5', '', 'CABULA', '41320480', 'SALVADOR', 'PA', 'Atividade individual', '', 0, 41),
(10, 'JULIA', 2, 0, '123122', '1978-06-09', '999387669', 'RUA JOAQUIM NAMBUCO DE ALMEIDA', '5', 'CASA', 'BARRA', '41320480', 'SALVADOR', 'PB', 'Atividade individual', '', 0, 41),
(11, 'DILMA SILVA MARTINS', 2, 0, '123456789', '1958-05-08', '71 55885577', 'RUA AUGUSTO BORTES', '22', 'EDF', 'BROTAS', '40275076', 'SALVASDOR', 'AC', '', '', 0, 41),
(15, 'LEDA VENA', 1, 0, '123479', '2017-05-08', '71 55885577', 'RUA AUGUSTO BORTES', '22', '', 'BROTAS', '84010330', 'SALVASDOR', 'AC', '', '', 0, 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blocos`
--

CREATE TABLE IF NOT EXISTS `blocos` (
  `id_bloco` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `st_responsavel` int(11) NOT NULL,
  `ck_trabalho` int(11) NOT NULL,
  `nm_foliao` varchar(30) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `dt_nascimento` date NOT NULL,
  `nu_rg_foliao` varchar(12) NOT NULL,
  `st_parentesco` int(11) NOT NULL,
  `te_obs` varchar(199) NOT NULL,
  `id_responsavel` int(11) NOT NULL,
  `nu_ano` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_bloco`),
  KEY `id_associado` (`id_associado`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Extraindo dados da tabela `blocos`
--

INSERT INTO `blocos` (`id_bloco`, `id_associado`, `st_responsavel`, `ck_trabalho`, `nm_foliao`, `id_sexo`, `dt_nascimento`, `nu_rg_foliao`, `st_parentesco`, `te_obs`, `id_responsavel`, `nu_ano`, `id_usuario`) VALUES
(27, 9, 1, 0, 'ELISANGELA', 2, '1978-06-09', '2049318359', 1, '', 0, 2019, 41),
(29, 10, 1, 0, 'ANTONIO ARRUELA', 1, '2005-08-09', '4549318666', 1, '', 0, 2019, 41),
(30, 9, 3, 0, 'HELENA DIAS ALMEIDA', 2, '2018-05-15', '1324688', 2, '', 0, 2019, 41),
(31, 9, 3, 0, 'ANTONIO JOSE PEREIRA', 1, '2018-05-09', '132468877', 2, '', 0, 2019, 41),
(32, 11, 0, 0, '', 0, '0000-00-00', '', 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `curriculos`
--

CREATE TABLE IF NOT EXISTS `curriculos` (
  `id_curriculo` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `nm_profissao` varchar(40) NOT NULL,
  `nm_instituicao` varchar(30) NOT NULL,
  `dt_curriculo` date NOT NULL,
  `st_curriculo` int(11) NOT NULL DEFAULT '1',
  `te_obs` varchar(200) NOT NULL,
  `te_arquivo` varchar(30) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_curriculo`),
  KEY `id_associado` (`id_associado`,`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `curriculos`
--

INSERT INTO `curriculos` (`id_curriculo`, `id_associado`, `nm_profissao`, `nm_instituicao`, `dt_curriculo`, `st_curriculo`, `te_obs`, `te_arquivo`, `id_usuario`) VALUES
(14, 9, 'DIRETORA', '', '0000-00-00', 1, 'Atividade individual', 'nailson_luciano.pdf', 41),
(15, 10, 'CARPINTEIRO', '', '0000-00-00', 1, 'Jornada pedagógica', 'curriculo-3.pdf', 41),
(16, 11, 'ANALISTA', '', '0000-00-00', 1, 'atividade (5.0)', 'rriculo-2.pdf', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id_curso` int(11) NOT NULL AUTO_INCREMENT,
  `nm_curso` varchar(30) NOT NULL,
  `st_controle` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_curso`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`id_curso`, `nm_curso`, `st_controle`, `id_usuario`) VALUES
(1, 'ELETRICISTA', 0, 41),
(3, 'BANCO DE DADOS', 0, 41),
(4, 'LOGICA DE PROGRAMAçãO', 0, 41),
(5, 'DANÇA DE SALÃO', 0, 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `dt_log` date NOT NULL,
  `hr_log` varchar(5) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_acao` int(11) NOT NULL,
  `nm_objeto` varchar(50) NOT NULL,
  `id_indice` int(11) NOT NULL,
  `nu_ip` varchar(50) NOT NULL,
  `nm_acesso` varchar(50) NOT NULL,
  PRIMARY KEY (`id_log`),
  KEY `id_usuario` (`id_usuario`,`id_acao`),
  KEY `id_indice` (`id_indice`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2635 ;

--
-- Extraindo dados da tabela `logs`
--

INSERT INTO `logs` (`id_log`, `dt_log`, `hr_log`, `id_usuario`, `id_acao`, `nm_objeto`, `id_indice`, `nu_ip`, `nm_acesso`) VALUES
(2599, '2018-11-08', '21:38', 41, 5, 'Login', 41, '::1', ''),
(2598, '2018-11-08', '21:25', 41, 5, 'Login', 41, '::1', ''),
(2597, '2018-11-07', '19:37', 41, 5, 'Login', 41, '::1', ''),
(2596, '2018-11-07', '23:29', 41, 5, 'Login', 41, '::1', ''),
(2595, '2018-11-07', '22:50', 41, 5, 'Login', 41, '::1', ''),
(2600, '2018-11-08', '11:29', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2601, '2018-11-08', '12:34', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2602, '2018-11-08', '12:36', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2603, '2018-11-09', '23:05', 41, 5, 'Login', 41, '::1', ''),
(2604, '2018-11-09', '23:34', 41, 5, 'Login', 41, '::1', ''),
(2605, '2018-11-09', '00:01', 41, 5, 'Login', 41, '::1', ''),
(2606, '2018-11-09', '00:06', 41, 5, 'Login', 41, '::1', ''),
(2607, '2018-11-09', '19:24', 41, 5, 'Login', 41, '::1', ''),
(2608, '2018-11-09', '19:48', 41, 5, 'Login', 41, '::1', ''),
(2609, '2018-11-10', '04:24', 41, 5, 'Login', 41, '::1', ''),
(2610, '2018-11-10', '04:57', 41, 5, 'Login', 41, '::1', ''),
(2611, '2018-11-10', '05:08', 41, 5, 'Login', 41, '::1', ''),
(2612, '2018-11-10', '18:05', 41, 5, 'Login', 41, '::1', ''),
(2613, '2018-11-10', '18:29', 41, 5, 'Login', 41, '::1', ''),
(2614, '2018-11-10', '18:43', 41, 5, 'Login', 41, '::1', ''),
(2615, '2018-11-11', '12:57', 41, 5, 'Login', 41, '::1', ''),
(2616, '2018-11-11', '15:24', 41, 5, 'Login', 41, '::1', ''),
(2617, '2018-11-11', '17:05', 41, 5, 'Login', 41, '::1', ''),
(2618, '2018-11-11', '18:03', 41, 5, 'Login', 41, '::1', ''),
(2619, '2018-11-11', '18:06', 41, 5, 'Login', 41, '::1', ''),
(2620, '2018-11-12', '22:59', 41, 5, 'Login', 41, '::1', ''),
(2621, '2018-11-12', '23:35', 41, 5, 'Login', 41, '::1', ''),
(2622, '2018-11-12', '00:01', 41, 5, 'Login', 41, '::1', ''),
(2623, '2018-11-12', '00:18', 41, 5, 'Login', 41, '::1', ''),
(2624, '2018-11-12', '07:38', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2625, '2018-11-12', '08:34', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2626, '2018-11-12', '12:54', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2627, '2018-11-12', '13:55', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2628, '2018-11-13', '07:49', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2629, '2018-11-13', '08:50', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2630, '2018-11-13', '12:15', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2631, '2018-11-13', '13:06', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2632, '2018-11-13', '14:37', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2633, '2018-11-14', '07:18', 41, 5, 'Login', 41, '127.0.0.1', ''),
(2634, '2018-11-14', '10:00', 41, 5, 'Login', 41, '127.0.0.1', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `psicanalistas`
--

CREATE TABLE IF NOT EXISTS `psicanalistas` (
  `id_psicanalista` int(11) NOT NULL AUTO_INCREMENT,
  `nm_psicanalista` varchar(30) NOT NULL,
  `te_conselho` varchar(10) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `te_imagem` varchar(20) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_psicanalista`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `psicanalistas`
--

INSERT INTO `psicanalistas` (`id_psicanalista`, `nm_psicanalista`, `te_conselho`, `id_sexo`, `te_imagem`, `id_usuario`) VALUES
(3, 'DRA ALZIRA MOREIRA CALAZANS', '7875/BA', 1, '', 0),
(4, 'DR WILLIAM AZEVEDO COSTA', '3427/RJ', 2, '', 0),
(5, 'HOMERO PARREIRA NUNES', '127875/BA', 1, '18_anos.JPG', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `psicos`
--

CREATE TABLE IF NOT EXISTS `psicos` (
  `id_psico` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `dt_inicio` date NOT NULL,
  `dt_fim` date NOT NULL,
  `hr_psico` varchar(5) NOT NULL,
  `nm_psico` varchar(30) NOT NULL,
  `id_psicanalista` int(11) NOT NULL,
  `te_obs` varchar(90) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_psico`),
  KEY `id_associado` (`id_associado`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_psicanalista` (`id_psicanalista`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Extraindo dados da tabela `psicos`
--

INSERT INTO `psicos` (`id_psico`, `id_associado`, `dt_inicio`, `dt_fim`, `hr_psico`, `nm_psico`, `id_psicanalista`, `te_obs`, `id_usuario`) VALUES
(20, 4, '2017-12-03', '2019-01-03', '08:00', '', 4, '', 41),
(21, 9, '2017-11-03', '2019-01-03', '08:00', '', 5, 'Atividade individual', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id_servico` int(11) NOT NULL AUTO_INCREMENT,
  `nm_servico` varchar(30) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_servico`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id_servico`, `nm_servico`, `id_usuario`) VALUES
(1, 'COMPROVANTE RESIDÊNCIA', 41),
(3, 'OUTROS', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacoes`
--

CREATE TABLE IF NOT EXISTS `solicitacoes` (
  `id_solicitacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `id_servico` int(11) NOT NULL,
  `nm_solicitante` varchar(30) NOT NULL,
  `nu_telefone_sol` varchar(20) NOT NULL,
  `dt_solicitacao` date NOT NULL,
  `nm_logra_sol` varchar(30) NOT NULL,
  `nu_logra_sol` varchar(5) NOT NULL,
  `nm_bairro_sol` varchar(20) NOT NULL,
  `nu_cep_sol` varchar(8) NOT NULL,
  `nm_cidade_sol` varchar(20) NOT NULL,
  `nm_uf_sol` varchar(2) NOT NULL,
  `te_obs_sol` varchar(90) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_solicitacao`),
  KEY `id_associado` (`id_associado`,`id_servico`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `solicitacoes`
--

INSERT INTO `solicitacoes` (`id_solicitacao`, `id_associado`, `id_servico`, `nm_solicitante`, `nu_telefone_sol`, `dt_solicitacao`, `nm_logra_sol`, `nu_logra_sol`, `nm_bairro_sol`, `nu_cep_sol`, `nm_cidade_sol`, `nm_uf_sol`, `te_obs_sol`, `id_usuario`) VALUES
(1, 11, 1, 'A MESMA', '(71) 5688-1244', '2018-11-14', 'RUA MARIA AUGUSTA MAIA', '', 'CAMPINAS DE BROTAS', '40275076', 'SALVADOR', 'BA', 'dsfsdfsdf', 41),
(2, 11, 1, 'A MESMA', '(71) 5688-1244', '2018-11-14', 'RUA MARIA AUGUSTA MAIA', '', 'CAMPINAS DE BROTAS', '40275076', 'SALVADOR', 'BA', 'vcxvv\\zxc\\', 41),
(3, 11, 1, 'A MESMA', '(71) 5688-1244', '2018-11-14', 'RUA MARIA AUGUSTA MAIA', '', 'CAMPINAS DE BROTAS', '40275076', 'SALVADOR', 'BA', 'bgg', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `nm_status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `status`
--

INSERT INTO `status` (`id_status`, `nm_status`) VALUES
(1, 'supervisor'),
(2, 'operador'),
(3, 'visitante'),
(4, 'desligado'),
(5, 'novo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `treinos`
--

CREATE TABLE IF NOT EXISTS `treinos` (
  `id_treino` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `nu_carga` int(11) NOT NULL,
  `dt_inicio` date NOT NULL,
  `dt_fim` date NOT NULL,
  `hr_treino` varchar(5) NOT NULL,
  `te_obs` varchar(200) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_treino`),
  KEY `id_associado` (`id_associado`,`id_curso`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `treinos`
--

INSERT INTO `treinos` (`id_treino`, `id_associado`, `id_curso`, `nu_carga`, `dt_inicio`, `dt_fim`, `hr_treino`, `te_obs`, `id_usuario`) VALUES
(4, 2, 5, 20, '2018-01-01', '2019-10-31', '10:00', '', 41),
(5, 9, 3, 20, '2017-11-03', '2019-01-03', '12:00', '', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nm_usuario` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nm_login` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nm_senha` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `nu_ip` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `id_sexo` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `nu_matricula` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `nu_telefone` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '3',
  `dt_cadastro` date NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_status` (`id_status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=57 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nm_usuario`, `nm_login`, `nm_senha`, `nu_ip`, `id_sexo`, `nu_matricula`, `nu_telefone`, `id_status`, `dt_cadastro`) VALUES
(40, 'Administrador', 'admin', '*2AF5EFAC0B14C5132BCD3D297954B53E634D160F', '', '', '', '', 1, '2017-11-04'),
(41, 'jairo vitorino', 'jairo.vitorino@gmail.com', '*CAC926C90985FC48783145FD428E3F0EBDCF43A5', '::1', '', '', '', 1, '2017-11-04'),
(56, 'hhhhh', 'hhhh@yyyy.com', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257', '::1', '', '', '', 1, '2018-11-01');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
