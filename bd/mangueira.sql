﻿-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 08/02/2019 às 04h29min
-- Versão do Servidor: 5.5.16
-- Versão do PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `mangueira`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acoes`
--

CREATE TABLE IF NOT EXISTS `acoes` (
  `id_acao` int(11) NOT NULL AUTO_INCREMENT,
  `nm_acao` varchar(20) NOT NULL,
  PRIMARY KEY (`id_acao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `acoes`
--

INSERT INTO `acoes` (`id_acao`, `nm_acao`) VALUES
(1, 'Inserir'),
(2, 'Editar'),
(3, 'Excluir'),
(4, 'Listar'),
(5, 'Logar');

-- --------------------------------------------------------

--
-- Estrutura da tabela `associados`
--

CREATE TABLE IF NOT EXISTS `associados` (
  `id_associado` int(11) NOT NULL AUTO_INCREMENT,
  `nm_associado` varchar(30) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `id_escolaridade` int(11) NOT NULL,
  `nu_rg` varchar(11) NOT NULL,
  `nu_cpf` varchar(11) NOT NULL,
  `dt_nascimento` date NOT NULL,
  `nu_telefone` varchar(30) NOT NULL,
  `nu_whatsapp` varchar(11) NOT NULL,
  `nm_logra` varchar(40) NOT NULL,
  `nu_logra` varchar(6) NOT NULL,
  `nm_compl` varchar(15) NOT NULL,
  `nm_bairro` varchar(20) NOT NULL,
  `nu_cep` varchar(8) NOT NULL,
  `nm_cidade` varchar(20) NOT NULL,
  `nm_uf` varchar(2) NOT NULL,
  `te_obs` varchar(250) NOT NULL,
  `te_imagem` varchar(30) NOT NULL,
  `st_controle` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_associado`),
  KEY `id_escolaridade` (`id_escolaridade`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `associados`
--

INSERT INTO `associados` (`id_associado`, `nm_associado`, `id_sexo`, `id_escolaridade`, `nu_rg`, `nu_cpf`, `dt_nascimento`, `nu_telefone`, `nu_whatsapp`, `nm_logra`, `nu_logra`, `nm_compl`, `nm_bairro`, `nu_cep`, `nm_cidade`, `nm_uf`, `te_obs`, `te_imagem`, `st_controle`, `id_usuario`) VALUES
(10, 'OLAVO DE CARVALHO', 1, 0, '', '02350499544', '1978-06-09', '999387669', '71981105701', 'RUA MARIA AUGUSTA MAIA', '44', '', 'CAMPINAS DE BROTAS', '40275076', 'SALVADOR', 'BA', '', 'rendimentos.pdf', 0, 41),
(11, 'HUMBER', 1, 0, '', '13959930511', '1978-06-09', '999387669', '', 'RUA MARIA AUGUSTA MAIA', '', '', 'CAMPINAS DE BROTAS', '40275076', 'SALVADOR', 'BA', '', '18_anos.JPG', 0, 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blocos`
--

CREATE TABLE IF NOT EXISTS `blocos` (
  `id_bloco` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `st_responsavel` int(11) NOT NULL,
  `ck_trabalho` int(11) NOT NULL,
  `nm_foliao` varchar(30) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `dt_nascimento` date NOT NULL,
  `nu_rg_foliao` varchar(12) NOT NULL,
  `st_parentesco` int(11) NOT NULL,
  `te_obs` varchar(199) NOT NULL,
  `id_responsavel` int(11) NOT NULL,
  `nu_ano` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_bloco`),
  KEY `id_associado` (`id_associado`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nm_categoria` varchar(30) NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `nm_categoria`) VALUES
(1, 'MÉDICO'),
(2, 'PSIQUIATRA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `curriculos`
--

CREATE TABLE IF NOT EXISTS `curriculos` (
  `id_curriculo` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `nm_profissao` varchar(80) NOT NULL,
  `nm_indicacao` varchar(30) NOT NULL,
  `nm_instituicao` varchar(30) NOT NULL,
  `dt_curriculo` date NOT NULL,
  `st_curriculo` int(11) NOT NULL DEFAULT '1',
  `te_obs` varchar(200) NOT NULL,
  `te_arquivo` varchar(100) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_curriculo`),
  KEY `id_associado` (`id_associado`,`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `curriculos`
--

INSERT INTO `curriculos` (`id_curriculo`, `id_associado`, `nm_profissao`, `nm_indicacao`, `nm_instituicao`, `dt_curriculo`, `st_curriculo`, `te_obs`, `te_arquivo`, `id_usuario`) VALUES
(1, 11, 'TESTE TESTE', 'TESTE', '', '0000-00-00', 1, '', 'C:\\fakepath\\nailson_luciano.pdf', 41),
(2, 10, 'TESTE TESTE', 'TESTE', '', '0000-00-00', 1, '', 'C:\\fakepath\\nailson_luciano.pdf', 41),
(4, 10, 'TESTE TESTE', 'TESTE', '', '0000-00-00', 1, '', 'C:\\fakepath\\itau_092017.pdf', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id_curso` int(11) NOT NULL AUTO_INCREMENT,
  `nm_curso` varchar(30) NOT NULL,
  `st_controle` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_curso`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`id_curso`, `nm_curso`, `st_controle`, `id_usuario`) VALUES
(1, 'ELETRICISTA', 0, 41),
(3, 'BANCO DE DADOS', 0, 41),
(4, 'LOGICA DE PROGRAMAçãO', 0, 41),
(5, 'DANÇA DE SALÃO', 0, 41),
(6, 'APINCULTURA', 0, 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `dt_log` date NOT NULL,
  `hr_log` varchar(5) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_acao` int(11) NOT NULL,
  `nm_objeto` varchar(50) NOT NULL,
  `nu_ip` varchar(50) NOT NULL,
  `nm_dispositivo` varchar(20) NOT NULL,
  PRIMARY KEY (`id_log`),
  KEY `id_usuario` (`id_usuario`,`id_acao`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2779 ;

--
-- Extraindo dados da tabela `logs`
--

INSERT INTO `logs` (`id_log`, `dt_log`, `hr_log`, `id_usuario`, `id_acao`, `nm_objeto`, `nu_ip`, `nm_dispositivo`) VALUES
(2764, '2019-02-02', '11:32', 41, 1, 'Associado', '::1', 'Computador'),
(2765, '2019-02-02', '11:33', 41, 3, 'Associado', '::1', 'Computador'),
(2766, '2019-02-02', '11:33', 41, 1, 'Associado', '::1', 'Computador'),
(2767, '2019-02-02', '12:38', 41, 5, 'Login', '::1', 'Computador'),
(2768, '2019-02-02', '12:38', 41, 1, 'Associado', '::1', 'Computador'),
(2769, '2019-02-02', '19:22', 41, 5, 'Login', '::1', 'Computador'),
(2770, '2019-02-02', '19:22', 40, 5, 'Login', '::1', 'Computador'),
(2771, '2019-02-02', '19:28', 40, 5, 'Login', '::1', 'Computador'),
(2772, '2019-02-03', '13:33', 41, 5, 'Login', '::1', 'Computador'),
(2773, '2019-02-05', '08:05', 41, 5, 'Login', '::1', 'Computador'),
(2774, '2019-02-05', '11:56', 41, 5, 'Login', '127.0.0.1', 'Computador'),
(2775, '2019-02-05', '11:59', 41, 1, 'Curriulo', '127.0.0.1', 'Computador'),
(2776, '2019-02-05', '12:24', 41, 1, 'Curriulo', '127.0.0.1', 'Computador'),
(2777, '2019-02-05', '12:26', 41, 1, 'Curriulo', '127.0.0.1', 'Computador'),
(2778, '2019-02-05', '12:34', 41, 1, 'Curriulo', '127.0.0.1', 'Computador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `psicanalistas`
--

CREATE TABLE IF NOT EXISTS `psicanalistas` (
  `id_psicanalista` int(11) NOT NULL AUTO_INCREMENT,
  `nm_psicanalista` varchar(30) NOT NULL,
  `te_conselho` varchar(10) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `te_imagem` varchar(20) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_psicanalista`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `psicanalistas`
--

INSERT INTO `psicanalistas` (`id_psicanalista`, `nm_psicanalista`, `te_conselho`, `id_sexo`, `te_imagem`, `id_usuario`) VALUES
(3, 'DRA ALZIRA MOREIRA CALAZANS', '7875/BA', 1, '', 0),
(4, 'DR WILLIAM AZEVEDO COSTA', '3427/RJ', 2, '', 0),
(5, 'HOMERO PARREIRA NUNES', '127875/BA', 1, '18_anos.JPG', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `psicos`
--

CREATE TABLE IF NOT EXISTS `psicos` (
  `id_psico` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `dt_inicio` date NOT NULL,
  `dt_fim` date NOT NULL,
  `hr_psico` varchar(5) NOT NULL,
  `nm_psico` varchar(30) NOT NULL,
  `id_psicanalista` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `te_obs` varchar(90) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_psico`),
  KEY `id_associado` (`id_associado`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_psicanalista` (`id_psicanalista`),
  KEY `id_categoria` (`id_categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id_servico` int(11) NOT NULL AUTO_INCREMENT,
  `nm_servico` varchar(22) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_servico`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id_servico`, `nm_servico`, `id_usuario`) VALUES
(1, 'COMPROVANTE RESIDÊNCIA', 41),
(5, 'ENCAMINHAMENTO', 41);

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacoes`
--

CREATE TABLE IF NOT EXISTS `solicitacoes` (
  `id_solicitacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `id_servico` int(11) NOT NULL,
  `nm_solicitante` varchar(30) NOT NULL,
  `nu_telefone_sol` varchar(20) NOT NULL,
  `dt_solicitacao` date NOT NULL,
  `nu_ano` int(11) NOT NULL,
  `nm_logra_sol` varchar(30) NOT NULL,
  `nu_logra_sol` varchar(5) NOT NULL,
  `nm_bairro_sol` varchar(20) NOT NULL,
  `nu_cep_sol` varchar(8) NOT NULL,
  `nm_cidade_sol` varchar(20) NOT NULL,
  `nm_uf_sol` varchar(2) NOT NULL,
  `te_obs_sol` varchar(90) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_solicitacao`),
  KEY `id_associado` (`id_associado`,`id_servico`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `nm_status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `status`
--

INSERT INTO `status` (`id_status`, `nm_status`) VALUES
(1, 'supervisor'),
(2, 'operador'),
(3, 'visitante'),
(4, 'desligado'),
(5, 'novo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `torpedos`
--

CREATE TABLE IF NOT EXISTS `torpedos` (
  `id_torpedo` int(11) NOT NULL AUTO_INCREMENT,
  `nu_torpedo` varchar(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_torpedo`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `treinos`
--

CREATE TABLE IF NOT EXISTS `treinos` (
  `id_treino` int(11) NOT NULL AUTO_INCREMENT,
  `id_associado` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `nu_carga` int(11) NOT NULL,
  `dt_inicio` date NOT NULL,
  `dt_fim` date NOT NULL,
  `hr_treino` varchar(5) NOT NULL,
  `te_obs` varchar(200) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_treino`),
  KEY `id_associado` (`id_associado`,`id_curso`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nm_usuario` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nm_login` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nm_senha` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `nu_ip` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `id_sexo` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `nu_matricula` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `nu_telefone` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '3',
  `dt_cadastro` date NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_status` (`id_status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=59 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nm_usuario`, `nm_login`, `nm_senha`, `nu_ip`, `id_sexo`, `nu_matricula`, `nu_telefone`, `id_status`, `dt_cadastro`) VALUES
(40, 'Administrador', 'admin', '*2AF5EFAC0B14C5132BCD3D297954B53E634D160F', '', '', '', '', 1, '2017-11-04'),
(41, 'jairo vitorino', 'jairo.vitorino@gmail.com', '*CAC926C90985FC48783145FD428E3F0EBDCF43A5', '::1', '', '', '', 1, '2017-11-04'),
(56, 'Não cadastrado', '', '', '::1', '', '', '', 1, '2018-11-01'),
(57, 'elaine', 'elaine@hotmail.com', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257', '::1', '', '', '', 2, '2018-12-02'),
(58, 'pedro almeida', 'pedro@gmail.com', '*DEF49BFB4C40BB39B66DCD7D7F3098817A6E5677', '::1', '', '', '', 2, '2019-02-02');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
